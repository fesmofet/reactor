import { connect } from 'react-redux';
import Component from './Header';
import * as actions from '@constants/actionCreators';


const mapStateToProps = (state) => ({
  dictionary: state.translates.dictionary,
});

const mapDispatchToProps = (dispatch) => ({
  toggleModalWindow: (payload) => dispatch(actions.toggleModalWindow(payload)),
});

export default connect(mapStateToProps, mapDispatchToProps)(Component);
