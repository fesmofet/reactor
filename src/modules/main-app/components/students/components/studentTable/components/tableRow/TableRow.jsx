import React from 'react';
import "./tableRow.less"

class TableRow extends React.Component {

    render() {
        const {item,  onUpdate, onDelete, updateText, deleteText, tableIndex} = this.props;
        return (
            <div className="table-row-wrapper">
                <span className="row-id">{tableIndex}</span>
                <span className="row-name">{item.fn}</span>
                <span className="row-surname">{item.ln}</span>
                <span className="row-age">{item.age}</span>
                <span className="row-city">{item.ht}</span>
                <div className="row-action">
                    <button onClick={() => onUpdate(item.id)}
                            className="row-button">{updateText}</button>
                    <button onClick={() => onDelete(item.id, item.fn, item.ln, item.age, item.ht)}
                            className="row-button">{deleteText}</button>
                </div>
            </div>
        )
    }
}

export default TableRow;
