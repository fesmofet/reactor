module.exports = {
  connectionString: 'postgres://postgres:admin@localhost:5432/students',
  pathAvatar: 'avatar.png',
  instructionEmpty: '',
  pathPg: {
    forgotPassword: '/password-forgot',
    getNewAvatar: '/get-new-avatar',
    authorizeTeacher: '/authorize-teacher',
    createTeacher: '/create-teacher',
    updateTeacher: '/update-teacher',
    saveGroupName: '/save-group-name',
    createStudent: '/create-student',
    updateStudent: '/update-student-info',
    deleteStudent: '/delete-student',
    deleteAllStudents: '/delete-all-students-from-group',
    deleteGroup: '/delete-group',
    createGroup: '/create-new-group',
    sendImage: '/sendImage',

    getGroups: '/teacher-groups',
    getStudents: '/all-students',
    getTeacher: '/teacher',
  },
  pathMongo: {
    url : 'mongodb+srv://boris:Dlj8lw0cZep4FBe5@cluster0-hnwd6.mongodb.net/test?retryWrites=true&w=majority',
    PORT : 5678,
    regData: '/',

    sessionData: '/add-session',
    addGeo: '/add-geo',

    studentsTimer: '/students-timer',
    paintTimer: '/paint-timer',
    calcTimer: '/calc-timer',
    accountTimer: '/account-timer',
    currencyTimer: '/currency-timer',
    converterTimer: '/converter-timer',

    createInc: '/click-create',
    updateInc: '/click-update',
    deleteInc: '/click-delete',
    clearInc: '/click-clear',
    chatInc: '/click-message',
    updateLogin: '/update-login',

    getData: '/render',
    

  },
};
