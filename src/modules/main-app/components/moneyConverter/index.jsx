import {connect} from "react-redux";
import Content from './MoneyConverter';

const mapStateToProps = state => ({
  
    dictionary: state.translates.dictionary,

});
const mapDispatchToProps = dispatch => ({

});

export default connect(mapStateToProps, mapDispatchToProps)(Content);
