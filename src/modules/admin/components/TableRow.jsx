import React from 'react';
import { checkMode, CheckButton } from '@utils/utils';

const TableRow = ({
    login, activeSessions, browser, ip, geo, device, chat, tStudents, tPaint, tCalc,
    tAccount, tCurrency, tConverter, createBtn, clearBtn, deleteBtn, updateBtn
}) => {
    
    return(
        <div className="admin-table-row">
            <div className="admin-table-row__item">
                {login}
            </div>
            <div className="admin-table-row__item">
                {activeSessions ? activeSessions: '0'}
            </div>
            <div className="admin-table-row__item">
               {checkMode(tStudents, tPaint, tCalc, tAccount, tCurrency, tConverter)}
            </div>
            <div className="admin-table-row__item">
                {CheckButton(createBtn, clearBtn, deleteBtn, updateBtn)}
            </div>
            <div className="admin-table-row__item">
                {chat ? 'True' : 'False'}
            </div>
            <div className="admin-table-row__item">
                {device ? device : '-'}
            </div>
            <div className="admin-table-row__item">
                {geo ? geo : '-'}
            </div>
            <div className="admin-table-row__item">
                {ip ? ip : '-'}
            </div>
            <div className="admin-table-row__item">
                {browser ? browser : '-'}
            </div>
        </div>
    )
}

export default React.memo(TableRow)