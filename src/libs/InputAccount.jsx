import React from "react";
import PropTypes from "prop-types";



const InputAccount = (props) => {
    const {type, id, className} = props;

    return (
        <div className={'input-acc'}>
                <input
                       id={id}
                       type={type}
                       className={className}
                />
        </div>
    )
};


InputAccount.propTypes = {
    id: PropTypes.string.isRequired,
    type: PropTypes.string.isRequired,
    className: PropTypes.string.isRequired,
};

export default InputAccount;

