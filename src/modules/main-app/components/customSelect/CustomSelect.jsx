import React from "react"
import "./customSelect.less"
import { useHistory } from "react-router-dom";

const CustomSelect = ({dictionary}) => {
    let history = useHistory();

    const handleClickStudents = () => {
        history.push("/main/students");
    }
    const handleClickPaint = () => {
        history.push("/main/paint");
    }
    const handleClickMoneyConverter = () => {
        history.push("/main/money-converter");
    }
    const handleClickLengthConverter = () => {
        history.push("/main/length-converter");
    }
    const handleClickCalculator = () => {
        history.push("/main/calculator");
    }
    const handleClickMode = (e) => {
      let isNone = document.querySelector('.sub-menu__list');
      isNone.style.display === 'block' ? isNone.style.display = 'none' : isNone.style.display = 'block';
    }
   
    return (
        <nav className="menu">
          <ul className="menu__list"  >
            <li className="menu__link">
              <button >{dictionary.resources.mode}</button>
              <ul className="sub-menu__list">
                <li className="sub-menu__link" onClick={handleClickStudents}>
                  {dictionary.resources.students}
                </li>
                <li className="sub-menu__link" onClick={handleClickPaint}>
                  {dictionary.resources.paint}
                </li>
                <li className="sub-menu__link">
                  {dictionary.resources.converter}
                  <ul className="sub-sub-menu__list">
                    <li className="sub-sub-menu__link" onClick={handleClickMoneyConverter}>
                      {dictionary.resources.сonverterM}
                    </li>
                    <li className="sub-sub-menu__link" onClick={handleClickLengthConverter}>
                      {dictionary.resources.converterL}
                    </li>
                  </ul>
                </li>
                <li className="sub-menu__link" onClick={handleClickCalculator}>
                  {dictionary.resources.calc}
                </li>
              </ul>
            </li>
          </ul>
        </nav>
    )
}

export default React.memo(CustomSelect)