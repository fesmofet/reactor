import * as logic from "../sample";

describe(`Test Converter logic`, () => {
    it(`5+5=10`, () => {
        const a = 5;
        let b = 5;
        let expected = 10;
        let functionResult = logic.sum(a, b);
        expect(expected).toEqual(functionResult);
    })
});
