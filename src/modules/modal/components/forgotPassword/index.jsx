import { connect } from 'react-redux';
import Component from './ForgotPassword.jsx';

const mapStateToProps = (state) => ({
  dictionary: state.translates.dictionary,
  customStyles: state.config.modalStyles,
  inputsForgottenPassword: state.config.inputsForgottenPassword,
});
const mapDispatchToProps = (dispatch) => ({
  toggleModalWindow: (payload) => dispatch({ type: 'TOGGLE_MODAL_WINDOW', payload }),
});

export default connect(mapStateToProps, mapDispatchToProps)(Component);
