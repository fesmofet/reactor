import React from 'react';
import { Authorization } from '../Authorization.jsx';
import InputAuth from '../components/Input';
import Adapter from 'enzyme-adapter-react-16';
import { configure, shallow } from 'enzyme';
configure({ adapter: new Adapter() });

describe('AuthRender', () => {

    let wrapper;
    beforeEach(() => {
         wrapper = shallow(<Authorization authorizationRequest />);m 
    });
    it('it should render InputAuth', () => {
        wrapper.setProps({inputsAuth})
        expect(wrapper.find(InputAuth)).toHaveLength(2);
    }); 
})