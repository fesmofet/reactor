import React from 'react';
import './moneyConverter.less'

export default class ConvertorBox extends React.Component {
    render() {
        const {placeholderText, convertInputRef, convertSelectRef, value, maxLength, callback, disabled} = this.props;
        return (
            <main id="currency" >
                <div className='convert-item'>
                    <input type='text' placeholder={placeholderText} ref={convertInputRef} defaultValue={value} maxLength={maxLength} onChange={callback} disabled={disabled} className="converter-input"/>
                    <select className='convert-item-select' ref={convertSelectRef}>
                        <option value='UAH'>UAH</option>
                        <option value='EUR'>EUR</option>
                        <option value='USD'>USD</option>
                        <option value='RUR'>RUR</option>
                    </select>
                </div>
            </main>
        )
    }
    
}
