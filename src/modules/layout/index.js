import { connect } from 'react-redux';
import Component from './Layout';
import * as actions from "@constants/actionCreators";

const mapStateToProps = (state) => ({
  isLoggedIn: state.authorize.isLoggedIn,
  modals: state.modals,
  teacherData: state.account.teacherData,
});

const mapDispatchToProps = (dispatch) => ({
  login: () => dispatch({ type: 'LOG_IN' }),
  logout: () => dispatch({ type: 'LOG_OUT' }),
  saveAccount: (payload) => dispatch(actions.saveAccount(payload))
});

export default connect(mapStateToProps, mapDispatchToProps)(Component);
