import React from 'react';
import { Field, reduxForm } from 'redux-form';

let ContactForm = (props) => {
  const { handleSubmit } = props;
  return (
    <form onSubmit={handleSubmit}>
      <div>
        <label htmlFor="login">Login</label>
        <Field id="login" placeholder="Login" name="login" component="input" type="text" />
      </div>
      <div>
        <label htmlFor="email">Email</label>
        <Field id="email" placeholder="E-mail" name="email" component="input" type="email" />
      </div>
      <button type="submit">Submit</button>
    </form>
  );
};

ContactForm = reduxForm({
  // a unique name for the form
  form: 'login',
})(ContactForm);

export default ContactForm;
