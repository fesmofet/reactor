import path from '@constants/path.js';
import axios from 'axios';

export const authorizationRequest = (login, password, callbackGetPermissionToLogin, callbackChangeLocation, getDataTeacherAuthorization) => {
  const xhr = new XMLHttpRequest();
  if (!login || !password) {
    return alert('Empty inputs!!!');
  }
  if (login === 'admin' && password === 'admin') {
    callbackGetPermissionToLogin();
    callbackChangeLocation.push('/admin');
    return;
  }
  const requestBody = {
    login,
    password,
    picture_url: '',
  };
  xhr.open('POST', `${path.server}authorize-teacher`);
  xhr.setRequestHeader('Content-type', 'application/json');
  xhr.onreadystatechange = function () {
    if (xhr.readyState === 4) {
      if (xhr.status === 501 && xhr.responseText === 'SERVER ERROR') {
        alert('wrong password or login');
      }
      if (xhr.status === 200) {
        const responseArray = JSON.parse(xhr.responseText);
        localStorage.setItem('UserID', responseArray[0].id);
        localStorage.setItem('UserLogin', responseArray[0].login);
        getDataTeacherAuthorization(responseArray[0]);
        callbackGetPermissionToLogin();
        callbackChangeLocation.push('/main/students');
      }
    }
  };
  xhr.send(JSON.stringify(requestBody));
};

export const registrationRequest = (values, callbackChangeLocation) => {
  if (values.login === 'admin') {
    alert("You can't register as admin"); // FIXME
    return;
  }
  const requestBody = { ...values, avatar: '' };
  // utils.sendRequest("POST", `${path.server}create-teacher`, requestBody, callbackChangeLocation); FIXME utils.sendRequest(method, url, data, callback)
  const xhr = new XMLHttpRequest();
  xhr.open('POST', `${path.server}create-teacher`);
  xhr.setRequestHeader('Content-type', 'application/json');
  xhr.onreadystatechange = function () {
    if (xhr.readyState === 4) {
      if (xhr.status === 200) {
        const responseArray = JSON.parse(xhr.responseText);
        axios.post(`${path.mongo}`, {
          _id: responseArray[0].id,
          login: responseArray[0].login,
        });
        callbackChangeLocation.push('/');
      }

      if (xhr.status === 502) {
        alert('This login already taken');
      }
    }
  };
  xhr.send(JSON.stringify(requestBody));
};

export const createStudent = () => {

};


export const getGroupsFromServer = (id) => axios.get(`${path.server}teacher-groups?id_teacher=${id}`);

export const getStudentsFromServerByGroupId = (groupId) => axios.get(`${path.server}all-students?id_group=${groupId}`);

export const deleteStudentById = (studentId) => axios.post(`${path.server}delete-student`, { studentId });

export const deleteGroupById = (groupId) => axios.post(`${path.server}delete-group`, { groupId });

export const updateStudentInfo = (id, fn, ln, age, ht) => axios.post(`${path.server}update-student-info`, {
  id, fn, ln, age, ht,
});

export const addNewGroup = (id) => axios.post(`${path.server}create-new-group`, { id });

export const clearGroup = (groupId) => axios.post(`${path.server}delete-all-students-from-group`, { groupId });

export const renameGroup = (id, name) => axios.post(`${path.server}save-group-name`, { id, name });

export const getTabsArrayByGroupIds = (groupIds) => groupIds.map((element, index) => ({
  name: element.name,
  groupId: element.id_group,
  sequenceNumber: index,
}));

export const getRenderData = (userId, saveState, activeTab) => {
  getGroupsFromServer(userId).then((response) => {
    const groupIds = response.data;
    const defaultGroupId = groupIds[activeTab].id_group;
    const tabs = getTabsArrayByGroupIds(groupIds);
    getStudentsFromServerByGroupId(defaultGroupId).then((response) => {
      saveState(tabs, response.data);
    });
  });
};

export const createNewStudent = (student) => axios.post(`${path.server}create-student`, { student });

export const getPasswordValue = (obj, forgottenRef, dictionary) => {
  const passwordValue = { login: obj.loginForgotten, keyword: obj.keywordForgotten };

  const xhr = new XMLHttpRequest();
  xhr.open('POST', `${path.server}password-forgot`);
  xhr.setRequestHeader('Content-type', 'application/json');
  xhr.send(JSON.stringify(passwordValue));
  xhr.onload = function () {
    if (obj.keywordForgotten === '' || obj.loginForgotten === '') {
      forgottenRef.loginForgotten.current.style = 'border-color: red;\n' + ' background-color: #FDD';
      forgottenRef.keywordForgotten.current.style = 'border-color: red;\n' + ' background-color: #FDD';
      forgottenRef.message.current.innerHTML = dictionary.resources.forgottenError;
    } else if (xhr.status === 401) {
      forgottenRef.loginForgotten.current.style = 'border-color: red;\n' + ' background-color: #FDD';
      forgottenRef.keywordForgotten.current.style = 'border-color: red;\n' + ' background-color: #FDD';
      forgottenRef.message.current.innerHTML = dictionary.resources.forgottenError;
    } else {
      forgottenRef.passwordRecover.current.value = JSON.parse(this.response)[0].password;
    }
  };
};

export const getStatistic = (saveStatistic) => {
  axios.get(`${path.mongo}render`).then((response) => saveStatistic(response.data));
};
export const resetAll = arr => {
  axios.post(`${path.server}reset-all`, { id : arr })
}
// CLick button
export const clickCounter = (to) => {
  const id = localStorage.getItem('UserID');
  axios.post(`${path.mongo}${to}`, { _id: id });
};
// send Timer
export const pageTimer = (time, to, page) => {
  const id = localStorage.getItem('UserID');
  axios.post(`${path.mongo}${to}`, { _id: id, [page]: time });
};

export const getTeacherFromServer = (id, save) => {
  axios.get(`${path.server}teacher?id=${id}`)
  .then((response) => save(response.data[0]));
};

export const updateTeacherInfo = (id, login, email, phone, about, picture_url) => axios.post(`${path.server}update-teacher`, {
  id, login, email, phone, about, picture_url
});

export const changeLogin =  login  => {
  const id = localStorage.getItem('UserID');
  axios.post(`${path.mongo}${path.updateLogin}`, { _id: id, login: login });
};
