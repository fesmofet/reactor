const WebSocket = require("ws");
const server = new WebSocket.Server({port: 4000}, () => {
    console.log("ws was started on port 4000")
});

let ACTIVE_LOGINS = [];
const CLIENTS = [];
const PRIVATE_ROOMS = [];

server.on('connection', ws => {

    ws.on("message", request => {
        request = JSON.parse(request)
        
        switch(request.command) {
            case "WS_SAVE_DATA":
                saveData(request, ws);
                CLIENTS.forEach( user => {
                    user.ws.send(JSON.stringify(ACTIVE_LOGINS));
                })
                break;
            case "USER_LOGOUT":
               
                ACTIVE_LOGINS = ACTIVE_LOGINS.filter(login => login !== request.login)
                CLIENTS.forEach( user => {
                    user.ws.send(JSON.stringify(ACTIVE_LOGINS));
                })
                break;
            case "INVITE_PRIVATE_ROOM":
                sendOnlyOne(request.message, request.login);
                break;
            case "SEND_GLOBAL":
                sendMessageToAll(request.message, request.login);
                break;
            case "SEND_ALL_IN_ROOM":
                sendInPrivateRoom(request.message, request.roomId);
                break;
            case "ACCEPT_PRIVATE_ROOM":
                PRIVATE_ROOMS.push({
                    users: getClientsArray(request.login2, request.login1),
                    roomId: Math.random(),
                });
                sendOnlyOne(request.message, request.login);
                break;
            default:
                sendAll(request.message);
        }
    });
    
});

const sendMessageToAll = (message, login) => {
    CLIENTS.forEach(client => {
        const messageObject = {
            login,
            message,
            command: "SEND_GLOBAL",
        }
        client.ws.send(JSON.stringify(messageObject));
    })
}

const getClientsArray = (login1, login2) => {
    return CLIENTS.map(client => client.login === login1 || client.login === login2);
};

const sendInPrivateRoom = (message, roomId) => {
    PRIVATE_ROOMS.forEach(privateRoom => {
        if (privateRoom.roomId === roomId) {
            privateRoom.users.forEach(user => {
                user.ws.send(message)
            })
        }
    })
};

const saveData = (request, ws)=> {
    if (!ACTIVE_LOGINS.includes(request.login)) {
        ACTIVE_LOGINS.push(request.login);
    }
    CLIENTS.push({
        id: request.id,
        login: request.login,
        session: request.sid,
        ws,
    });

};

const sendAll = message => {
    CLIENTS.forEach(client => {
        client.ws.send("Message: " + message);
    })
};

const sendOnlyOne = (message, login) => {
    CLIENTS.forEach(client => {
        if (client.login === login) {
            client.ws.send({
                message,
                command: "CREATE_PRIVATE_ROOM"
            });
        }
    })
};
