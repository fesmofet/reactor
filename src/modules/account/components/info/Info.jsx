import React from "react";
import './info.less';
const Info = ({dictionary, teacherData, changeButtonAccount}) => {

    return (
        <div className="my-account-info">
            <div className="my-account-info__values">
                <label>{dictionary.resources.login}</label>
                <input
                    disabled='disabled'
                    className="my-account-info__values"
                    value={ teacherData === null ? null : teacherData.login }
                />
            </div>
            <div className="my-account-info__values">
                <label>{dictionary.resources.email}</label>
                <input
                    disabled='disabled'
                    className="my-account-info__values"
                    value={ teacherData === null ? null : teacherData.email }
                />
            </div>
            <div className="my-account-info__values">
                <label>{dictionary.resources.phone}</label>
                <input
                    disabled='disabled'
                    className="my-account-info__values"
                    value={ teacherData === null ? null : teacherData.phone }
                />
            </div>
            <div className="my-account-info__textarea">
                <label>{dictionary.resources.about}</label>
                <textarea
                    disabled='disabled'
                    defaultValue={teacherData.about}>
                </textarea>
            </div>
            <div className="my-account-info__button">
                <button onClick={changeButtonAccount}>{dictionary.resources.btnChange}</button>
            </div>
        </div>
    )
};

export default Info;