import React from 'react';
import PropTypes from 'prop-types';
import LengthConverter from './LengthConverter';
import './lengthConverter.less';
import Header from '../header/index'
import { pageTimer } from '@/utils/requests';
import path from "@constants/path"


export default class ConvertDates extends React.Component {
    converterCount;
    count = 0;
    state = {
        outputValue: ''
    };

    convRef = {
        inputValue: React.createRef(),
        outputValue: React.createRef(),
        inputSelect: React.createRef(),
        outputSelect: React.createRef(),
        convertBtn: React.createRef()
    };

    selectBox = {
        meter: 1,
        verst: 1067,
        mile: 1609,
        foot: 0.3047851264858275,
        yard: 0.9144,
    };

    checkEnterNumber = event => {
        event.target.value =  event.target.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');
    };

    calculate = () => {
        let convertToMeter = () => this.convRef.inputValue.current.value * this.selectBox[this.convRef.inputSelect.current.value];
        let convertFromMeter = (value) => value / this.selectBox[this.convRef.outputSelect.current.value];
        let result = convertFromMeter(convertToMeter());
        this.setState({
            outputValue: result
        });
    };
    
    componentDidMount() {
        this.converterCount = setInterval(() => {
            this.count++
        }, 10)
    }
    componentWillUnmount() {
        clearInterval(this.converterCount)
        pageTimer(this.count, path.converterTimer, path.timeConverter)
    }
    render() {
        const {dictionary , history} = this.props;
        return (
            <>
            <Header history={history}/>
            <main className='wrapper-main-conv'>
                <h1>{dictionary.resources.converterLength}</h1>
                <div className='content-box-conv'>
                    <LengthConverter
                        dictionary={dictionary}
                        placeholderText={dictionary.placeholders.converterLength}
                        convertInputRef={this.convRef.inputValue}
                        convertSelectRef={this.convRef.inputSelect}
                        maxLength={10}
                        callback={event => this.checkEnterNumber(event)}
                        meter={dictionary.resources.meter} />

                    <LengthConverter
                        dictionary={dictionary}
                        convertInputRef={this.convRef.outputValue}
                        convertSelectRef={this.convRef.outputSelect}
                        value={this.state.outputValue}
                        meter={dictionary.resources.meter}
                        disabled={"disabled"} />
                </div>
                <div>
                    <button className='convBtn' ref={this.convRef.convertBtn} onClick={this.calculate}>{dictionary.resources.convert}</button>
                </div>
            </main>
            </>
        )
    }
}