import React from 'react';
import * as requests from '../../../../../../utils/requests'
import CreateStudentForm from "./CreateStudentForm"
import "./controlPanel.less"
import path from "@constants/path"

export default class ControlPanel extends React.Component {
   
    handleCreateNewStudent = (name, surname, age, city) => {
        
        let student = {
            fn: name,
            ln: surname,
            age: age,
            ht: city,
            id_group: this.props.tabs[this.props.activeTab].groupId,
        };

        requests.createNewStudent(student).then(() => {requests.getRenderData(this.props.teacherData.id, this.saveData, this.props.activeTab)});
        requests.clickCounter(path.createCl);
    };

    handleClearGroup = (event) => {
        event.stopPropagation();
        let deleteTabConfirmationText = this.props.dictionary.dialogs.clearTable;
        if (window.confirm(deleteTabConfirmationText)) {
            requests.clearGroup(this.props.tabs[this.props.activeTab].groupId)
                .then(() => {
                    requests.getRenderData(this.props.teacherData.id, this.saveData, this.props.activeTab)
                })
        }
        requests.clickCounter(path.clearCl);
    };

    saveData = (tabs, students) => {
        this.props.addTabs(tabs);
        this.props.addStudents(students);
    };
   
    render() {
        const { dictionary , addNotification} = this.props
     
        return (
            <div className="students-menu-wrapper">
                <span className="control-panel-text">{dictionary.dialogs.controlPanel}</span>
                <span className="control-panel-text">{dictionary.dialogs.addStudent}</span>
                <CreateStudentForm {...this.props} 
                    dictionary={dictionary} 
                    handleCreateNewStudent={this.handleCreateNewStudent} 
                    handleClearGroup={this.handleClearGroup}/>
            </div>
        )
    }
}


