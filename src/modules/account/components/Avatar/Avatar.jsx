import React from 'react';
import './avatar.less';
import ContextMenu from 'react-context-menu';
import path from "@constants/path";


export default class Photo extends React.Component {

    sendImage = () => {
        let files = document.getElementById('file').files;
        if (files.length > 0) {
            this.getBase64(files[0]);
        }
    };

    getBase64 = (file) => {
        const {changePhoto} = this.props;
        let reader = new FileReader();
        reader.readAsDataURL(file);
        reader.onload = function () {
            let body = {
                img: reader.result,
                id: Number(localStorage.getItem('UserID')),
            };
            if (file.type !== "image/png" && file.type !== "image/jpeg" && file.type !== "image/svg") {
                alert("you can insert image only jpeg/png/svg format");
                return false
            }
            let xhr = new XMLHttpRequest();
            xhr.open("POST", `${path.server}sendImage`);
            xhr.setRequestHeader("Content-type", "application/json");
            xhr.send(JSON.stringify(body));
            xhr.onreadystatechange = function () {
                if (xhr.status === 400) {
                    console.log('Error')
                } else if(xhr.status === 200){
                    document.querySelector('.photo').style.backgroundImage = `url(${body.img.toString()})`;
                    changePhoto(JSON.stringify(body.img));
                }
            };
        };
        reader.onerror = function (error) {
            console.log('Error: ', error);
        };
    };

    fileInput = () => {
        document.getElementById('file').click();
    };

    render() {
        const {teacherInfo} = this.props;
        return (
            <div className='photo' id='test' style={ teacherInfo.picture_url != '' ?  {backgroundImage: `url(${teacherInfo.picture_url}`} : null}>
                <input id="file" type="file" onChange={() => this.sendImage()} hidden/>
                <ContextMenu
                    contextId={'test'}
                    items={[
                        {
                            label: 'Load Avatar',
                            onClick: () => this.fileInput()
                        }
                    ]} />
            </div>
        );
    }
}

