import React from "react";
import { pageTimer } from '@/utils/requests';
import path from "@constants/path"
import Header from '../header/index'
import ConvertorBox from './ConvertorBox'

export default class MoneyConverter extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            outputValueResult: '',
            error: null,
            currency: []
        };
        this.convRef = {
            inputValue: React.createRef(),
            outputValue: React.createRef(),
            inputSelect: React.createRef(),
            outputSelect: React.createRef(),
            convertBtn: React.createRef()
        };
    }
    currencyCount;
    count = 0;
    componentDidMount() {
        this.currencyCount = setInterval(() => {
            this.count++
        }, 10)
        fetch('https://api.privatbank.ua/p24api/pubinfo?json&exchange&coursid=5')
        .then(res => res.json())
        .then(
            (result) => {
                this.setState({
                    currency: result
                });
            },
            // Примечание: важно обрабатывать ошибки именно здесь, а не в блоке catch(),
            // чтобы не перехватывать исключения из ошибок в самих компонентах.
            (error) => {
                this.setState({
                    error
                });
            }
        )
    }

    countTheCurrency = () => {
        let result = null;

        if (this.convRef.inputSelect.current.value !== this.convRef.outputSelect.current.value) {
            if (this.convRef.inputSelect.current.value === 'UAH') {
                for (let i = 0; i < this.state.currency.length; i++) {
                    if (this.state.currency[i].ccy === this.convRef.outputSelect.current.value) {
                        result = this.convRef.inputValue.current.value / this.state.currency[i].sale;
                        this.setState({
                            outputValueResult: result.toFixed(4),
                        });
                    }
                }
            } else if (this.convRef.outputSelect.current.value === 'UAH') {
                for (let i = 0; i < this.state.currency.length; i++) {
                    if (this.state.currency[i].ccy === this.convRef.inputSelect.current.value) {
                        result = this.convRef.inputValue.current.value * this.state.currency[i].sale;
                        this.setState({
                            outputValueResult: result.toFixed(4),
                        });
                    }
                }
            } else {
                for (let i = 0; i < this.state.currency.length; i++) {
                    if (this.state.currency[i].ccy === this.convRef.inputSelect.current.value) {
                        result = this.convRef.inputValue.current.value * this.state.currency[i].buy;
                        this.setState({
                            outputValueResult: result.toFixed(4),
                        });
                    }
                }
                for (let i = 0; i < this.state.currency.length; i++) {
                    if (this.state.currency[i].ccy === this.convRef.outputSelect.current.value) {
                        result = result / this.state.currency[i].sale;
                        this.setState({
                            outputValueResult: result.toFixed(4),
                        });
                    }
                }
            }
        } else if (this.convRef.inputSelect.current.value === this.convRef.outputSelect.current.value) {
            this.setState({
                outputValueResult: this.convRef.inputValue.current.value,
            });
        }

    };


    checkEnterNumber = event => {
        event.target.value =  event.target.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');
    };

    componentWillUnmount() {
        clearInterval(this.currencyCount)
        pageTimer(this.count, path.currencyTimer, path.timeCurrency)
    }    
    render() {
        const { history, dictionary } = this.props
        return (
            <>
            <Header history={history}/>
            <main id="wrapper-main-conv" className='wrapper-main-conv'>
                <h1>{dictionary.resources.сonverterM}</h1>
                <div className='content-box-conv'>
                    <ConvertorBox
                        placeholderText={dictionary.placeholders.converterLength}
                        convertInputRef={this.convRef.inputValue}
                        convertSelectRef={this.convRef.inputSelect}
                        maxLength={10}
                        callback={event => this.checkEnterNumber(event)} />


                    <ConvertorBox
                        convertInputRef={this.convRef.outputValue}
                        convertSelectRef={this.convRef.outputSelect}
                        value={this.state.outputValueResult}
                        meter={dictionary.resources.meter}
                        disabled={"disabled"} />
                </div>
                <div>
                    <button className='convBtn' ref={this.convRef.convertBtn} onClick={this.countTheCurrency} >{dictionary.resources.convert}</button>
                </div>
            </main>
            </>
        )
    }
    
}


