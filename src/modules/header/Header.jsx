import React from 'react';
import Logo from './components/Logo';
import ControlButtons from './components/ControlButtons';
import './Header.less';

export default class Header extends React.Component {
  toggleSettingsModalWindow = () => {
    const { toggleModalWindow } = this.props;
    toggleModalWindow({ type: 'settings', data: null });
  };

  render() {
    return (
      <div className="header">
        <nav>
          <Logo className="header__logo" alt="" src="./assets/logo.png" />
          <ControlButtons
            className="header__control-buttons control-buttons"
            buttons={[
              {
                id: 1,
                className: 'control-buttons__settings',
                callback: this.toggleSettingsModalWindow,
              },
            ]}
          />
        </nav>
      </div>
    );
  }
}
