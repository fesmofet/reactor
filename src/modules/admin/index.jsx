import { connect } from 'react-redux';
import Component from './Admin';

import * as actions from '@constants/actionCreators';

const mapStateToProps = (state) => ({
  isLoggedIn: state.authorize.isLoggedIn,
  inputsAuth: state.config.inputsAuth,
  buttons: state.config.buttonAuthorization,
  dictionary: state.translates.dictionary,
  users: state.statistic.users
});

const mapDispatchToProps = (dispatch) => ({
  logout: () => dispatch(actions.logout()),
  saveStatistic: payload =>dispatch(actions.saveStatistic(payload))
});

export default connect(mapStateToProps, mapDispatchToProps)(Component);
