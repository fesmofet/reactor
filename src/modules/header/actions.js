import actionTypes from '../../constants/actionTypes';

export const toggleModalWindow = payload => ({ type: actionTypes.TOGGLE_MODAL_WINDOW, payload });
