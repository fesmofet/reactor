import React, {Component} from 'react';
import './account.less';
import Game from "./components/game/Game";
import './index';
import path from '@constants/path';
import Avatar from './components/avatar/Avatar';
import {pageTimer, getTeacherFromServer, updateTeacherInfo, changeLogin} from '@/utils/requests';
import Chat from './components/chat/index';
import Header from './components/header/index'
import Info from "@modules/account/components/info/Info";
import AccountForm from "@modules/account/components/accountForm/AccountForm";
export default class Account extends Component {
    accountCount;

    count = 0;

    componentDidMount() {
        this.accountCount = setInterval(() => {
            this.count++;
        }, 10);
        const id = localStorage.getItem("UserID");
        getTeacherFromServer(id, this.props.saveAccount);
    }


    componentWillUnmount() {
        clearInterval(this.accountCount);
        pageTimer(this.count, path.accountTimer, path.timeAccount);
    }


    handleSaveTeacherInfo = (id, login, email, phone, about, picture_url) => {
        id = localStorage.getItem("UserID");
        updateTeacherInfo(id, login, email, phone, about, picture_url);
        localStorage.setItem('UserLogin', login);
        changeLogin(login);
        location.reload() //FIXME
    };

    render() {
        const {inputsAcc, dictionary, teacherData, history, changePhoto} = this.props;
        return (
      <>
      <Header history={history}/>
            <div className="account-wrapper">
                <div className="first-block">
                    <Avatar
                        teacherInfo={teacherData}
                        changePhoto={changePhoto}
                    />
                    {this.props.buttonStateAbout
                        ?
                        <Info
                        dictionary={dictionary}
                        teacherData={teacherData}
                        changeButtonAccount={this.props.changeButtonAccount}
                    />
                    :
                        <AccountForm
                        dictionary={dictionary}
                        teacherData={teacherData}
                        saveTeacherData={this.handleSaveTeacherInfo}
                        changeButtonAccount={this.props.changeButtonAccount}
                        />
                    }
                </div>
                <div className="second-block">
                    <div className="second-block__chat">
                        <Chat/>
                    </div>
                    <div className="second-block__game">
                        <Game />
                    </div>
                </div>
            </div>
      </>
        );
    }
}
