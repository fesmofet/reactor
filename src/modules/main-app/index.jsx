import Component from "./MainApp";
import {connect} from "react-redux";
import * as actions  from "../../constants/actionCreators";

const mapStateToProps = state => ({
    isLoggedIn: state.authorize.isLoggedIn,
    dictionary: state.translates.dictionary,

});

const mapDispatchToProps = dispatch => ({
    login: () => dispatch(actions.login()),
    logout: () => dispatch(actions.logout()),
    currentLogin: payload => dispatch(actions.currentLogin(payload)),
    addUser: payload => dispatch(actions.addUser(payload)),
    saveAccount: (payload) => dispatch(actions.saveAccount(payload))
});

export default connect(mapStateToProps, mapDispatchToProps)(Component);