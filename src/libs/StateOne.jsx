import React from "react";
import PropTypes from "prop-types";
import Component from "../modules/account/Account.jsx";
import config from "@config/config";

export default class  StateOne extends React.Component {


    render() {
        const {labelText, id, type, className} = this.props;
        return (
            <div className="data">
                <label htmlFor={id}>
                    {[labelText]}
                    <input
                        id={id}
                        type={type}
                        className={className}
                    />
                </label>
            </div>
        )
    }
}