import ButtonInputs from "@libs/button/ButtonInputs";
import {connect} from "react-redux";
import * as actions from "./actions";

const mapStateToProps = state => ({
    buttonStateInput: state.account.buttonStateInput,
});

const mapDispatchToProps = dispatch => ({
    changeButtonInput: () => dispatch(actions.changeButtonInput()),
});

export default connect(mapStateToProps, mapDispatchToProps)(ButtonInputs)