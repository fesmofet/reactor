import React from "react";



export default class LengthConverter extends React.Component {
    
    render() {
        const { placeholderText, convertInputRef, convertSelectRef, value, maxLength, callback, disabled, dictionary } = this.props
        return (
            <div>
            
                <div className="wrapper_convert">
                    <input type='text' placeholder={placeholderText} ref={convertInputRef} defaultValue={value} maxLength={maxLength} onChange={callback} disabled={disabled} className="converter-input"/>
                    <select className='convert-item-select' ref={convertSelectRef}>
                        <option value='meter'>{dictionary.resources.meter}</option>
                        <option value='verst'>{dictionary.resources.verst}</option>
                        <option value='mile'>{dictionary.resources.mile}</option>
                        <option value='foot'>{dictionary.resources.foot}</option>
                        <option value='yard'>{dictionary.resources.yard}</option>
                    </select>
                </div>
            </div>
        )
    }
    
}

