import React from 'react';
import StudentsTable from "./components/studentTable/index";
import ControlPanel from "./components/controlPanel/index"
import Header from '../header/index'
import { pageTimer } from '@/utils/requests';
import path from "@constants/path"
import "./students.less"
import NotificationSystem from 'react-notification-system';

export default class Students extends React.Component {
    addStudentNotification = React.createRef();
    deleteStudentNotification = React.createRef();
    updateStudentNotification = React.createRef();

    addNotification = (event, name, surname, age, city) => {
      if(this.props.modals.notificationOff){
        return;
      }
      event.preventDefault();
      const notification = this.addStudentNotification.current;
      notification.addNotification({
        message: `${this.props.dictionary.dialogs.createStudent} ${name} ${surname} ${age} ${city}`,
        level: 'info',
        position: 'bl'
      });
    };
    deleteNotification = (event, name, surname, age, city) => {
      if(this.props.modals.notificationOff){
        return;
      }
      event.preventDefault();
      const notification = this.addStudentNotification.current;
      notification.addNotification({
        message: `${this.props.dictionary.dialogs.deleteStudent} ${name} ${surname} ${age} ${city}`,
        level: 'error',
        position: 'bl'
      });
    };
    updateNotification = (
        event, name, surname, age, city,
        newName, newSurname, newAge, newCity
        ) => {
        if(this.props.modals.notificationOff){
          return;
        }
        newName ? newName : newName = name;
        newSurname ? newSurname : newSurname = surname;
        newAge ? newAge : newAge = age;
        newCity ? newCity : newCity = city;
        event.preventDefault();
        const notification = this.updateStudentNotification.current;
        notification.addNotification({
          message: `${this.props.dictionary.dialogs.updateStudent} ${name} ${surname} ${age} ${city} 
          ${this.props.dictionary.dialogs.to} ${newName} ${newSurname} ${newAge} ${newCity}`,
          level: 'success',
          position: 'bl',
          autoDismiss: 10,
        });
    };
    studentsCount;
    count = 0;
    componentDidMount() {
        this.studentsCount = setInterval(() => {
            this.count++
        }, 10)
    }
    componentWillUnmount() {
        clearInterval(this.studentsCount)
        pageTimer(this.count, path.studentsTimer, path.timeStudents)
    }
    render() {
        const { history } = this.props
        return (
            <>
            <NotificationSystem ref={this.addStudentNotification} />
            <NotificationSystem ref={this.deleteStudentNotification} />
            <NotificationSystem ref={this.updateStudentNotification} />
            <Header history={history}/>
            <div className="students-wrapper">
            <div className="control-panel">
                    <ControlPanel addNotification={this.addNotification}/>
                </div>
                <div className="table-wrapper">
                    <StudentsTable 
                        deleteNotification={this.deleteNotification}
                        updateNotification={this.updateNotification}
                    />
                </div>
            </div>
            </>
        )
    }
}


