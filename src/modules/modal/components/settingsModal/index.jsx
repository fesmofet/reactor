import { connect } from 'react-redux';
import Component from './ModalSettings';
const mapStateToProps = (state) => ({
  options: state.config.options,
  dictionary: state.translates.dictionary,
  customStyles: state.config.modalStyles,
  defaultLocale: state.translates.locale,
});
const mapDispatchToProps = (dispatch) => ({
  changeLocale: (payload) => dispatch({ type: 'CHANGE_LOCALE', payload }),
  toggleModalWindow: (payload) => dispatch({ type: 'TOGGLE_MODAL_WINDOW', payload }),
});

export default connect(mapStateToProps, mapDispatchToProps)(Component);
