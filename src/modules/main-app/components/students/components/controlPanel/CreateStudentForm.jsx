import React from 'react';
import { withFormik, Form, Field } from 'formik';
import * as Yup from 'yup';


const CreateStudentForm = ({
  handleClearGroup, errors, touched, dictionary,
}) => (
  <Form className="registration-form">
    <div className="validation-input">
      <label htmlFor="createSurname">{dictionary.resources.name}</label>
      <Field maxLength="50" id="createSurname" type="text" name="name" placeholder={dictionary.placeholders.name} />
      {touched.name && errors.name && <p className="validation-error">{errors.name}</p>}
    </div>
    <div className="validation-input">
      <label htmlFor="createSurname">{dictionary.resources.surname}</label>
      <Field maxLength="50" id="createSurname" type="text" name="surname" placeholder={dictionary.placeholders.surname} />
      {touched.surname && errors.surname && <p className="validation-error">{errors.surname}</p>}
    </div>
    <div className="validation-input">
      <label htmlFor="createAge">{dictionary.resources.age}</label>
      <Field maxLength="50" id="createAge" type="text" name="age" placeholder={dictionary.placeholders.age} />
      {touched.age && errors.age && <p className="validation-error">{errors.age}</p>}
    </div>
    <div className="validation-input">
      <label htmlFor="createCity">{dictionary.resources.city}</label>
      <Field maxLength="50" id="createCity" type="text" name="city" placeholder={dictionary.placeholders.city} />
      {touched.city && errors.city && <p className="validation-error">{errors.city}</p>}
    </div>

    <button type="submit">{dictionary.resources.create}</button>
    <button type="button" onClick={handleClearGroup}>{dictionary.resources.clear}</button>
  </Form>
);
const FormikApp = withFormik({
  mapPropsToValues({
    name, surname, age, city,
  }) {
    return {
      name: name || '',
      surname: surname || '',
      age: age || '',
      city: city || '',
    };
  },
  handleSubmit(values, { props }) {
    props.handleCreateNewStudent(values.name, values.surname, values.age, values.city);
    props.addNotification(event, values.name, values.surname, values.age, values.city)
  },
  validationSchema: (props) => Yup.object().shape({
    name: Yup.string()
      .matches(/^[a-zA-Zа-яА-Я]+$/, props.dictionary.validation.onlyLetters)
      .required(props.dictionary.validation.required),
    surname: Yup.string()
      .matches(/^[a-zA-Zа-яА-Я]+$/, props.dictionary.validation.onlyLetters)
      .required(props.dictionary.validation.required),
    age: Yup.string()
      .matches(/^(?![0])/, props.dictionary.validation.ageZero)
      .matches(/^[0-9]+$/, props.dictionary.validation.onlyNumbers)
      .matches(/^[0-9][0-9]?$|^100$/, props.dictionary.validation.age)
      .required(props.dictionary.validation.required),
    city: Yup.string()
      .matches(/^[a-zA-Zа-яА-Я]+$/, props.dictionary.validation.onlyLetters)
      .required(props.dictionary.validation.required),
  }),
})(CreateStudentForm);

export default React.memo(FormikApp);
