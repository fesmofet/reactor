import Avatar from "./Avatar";
import {connect} from "react-redux";
import * as actions from "./actions";

const mapStateToProps = state => ({
    buttonStateInput: state.account.buttonStateInput,
    inputsAcc:state.config.inputsAcc,
});

const mapDispatchToProps = dispatch => ({
    changePhoto: payload => dispatch(actions.changePhoto(payload))
});

export default connect(mapStateToProps, mapDispatchToProps)(Avatar)