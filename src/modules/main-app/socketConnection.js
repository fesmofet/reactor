export class SocketConnection {
    static socket;
    constructor(path) {
      if (SocketConnection.socket) {
        return this;
      }
      SocketConnection.socket = new WebSocket(path);
      
    }
  
    static getSocket() {
      return SocketConnection.socket;
    }
  }