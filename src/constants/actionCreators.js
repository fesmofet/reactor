import action from './actionTypes';
import actionTypes from "@constants/actionTypes";

export const changeActiveTab = payload => ({type: 'CHANGE_ACTIVE_TAB', payload});
export const deleteTab = payload => ({type: 'DELETE_TAB', payload});
export const cancelUpdate = payload => ({type: 'CANCEL_UPDATE', payload});
export const updateTableRow = payload => ({type: 'UPDATE_TABLE_ROW', payload});
export const addTabs = payload => ({type: 'ADD_TABS', payload});
export const addStudents = payload => ({type: 'ADD_STUDENTS', payload});
export const setTabAsRenamed = payload => ({type: 'SET_TAB_AS_RENAMED', payload});
export const completeTabRenamed = payload => ({type: 'COMPLETE_TAB_RENAME', payload});
export const getDataTeacherAuthorization = payload => ( {type: 'GET_DATA_TEACHER', payload});
export const addUser = payload => ({type: 'ADD_USER', payload});
export const addMessage = payload => ({type: 'ADD_MESSAGE', payload});
export const currentLogin = payload => ({type: 'SET_CURRENT_LOGIN', payload});
export const toggleIsPrivateMode = () => ({type: 'TOGGLE_IS_PRIVATE_MODE'});
export const logout = () => ({type: 'LOG_OUT'});
export const login = () => ({type: 'LOG_IN'});
export const toggleModalWindow = payload => ({ type: 'TOGGLE_MODAL_WINDOW', payload });
export const saveStatistic = payload => ({ type: 'SAVE_STATISTIC', payload});
export const saveAccount = payload => ({ type: action.SAVE_ACCOUNT_INFO, payload });
export const toggleNotification = payload => ({type: 'TOGGLE_NOTIFICATION', payload});
export const changeButtonAccount = () => ({type: action.CHANGE_BUTTON_ABOUT});
export const changePhoto = payload => ({ type: actionTypes.CHANGE_PHOTO, payload});

