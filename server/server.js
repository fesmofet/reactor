const config = require('./static/config.js');
const helper = require('./helpers/serverHelpers.js');
const constants =require('./static/constants');
const bodyParser = require('body-parser');
const {Client} = require('pg');
const express = require('express');
const server = express();
const client = new Client(config.connectionString);

client.connect();

server.use(bodyParser.json({limit: '0.5mb', extended: true}));
server.use((req, res, next) => helper.cors(req, res, next));

server.listen(3001, function () {
    console.log('Server started on port 3001 ..')
});

server.post(constants.pathPg.forgotPassword, (req, res) => helper.forgotPassword(req, res, client));
server.post(constants.pathPg.getNewAvatar, (req, res) => helper.getNewAvatar(req, res, client));
server.post(constants.pathPg.authorizeTeacher, (req, res) => helper.authorizeTeacher(req, res, client));
server.post(constants.pathPg.createTeacher, (req, res) => helper.createTeacher(req, res, client));
server.post(constants.pathPg.updateTeacher, (req, res) => helper.updateTeacher(req, res, client));
server.post(constants.pathPg.saveGroupName, (req, res) => helper.saveGroupName(req, res, client));
server.post(constants.pathPg.createStudent, (req, res) => helper.createStudent(req, res, client));
server.post(constants.pathPg.updateStudent, (req, res) => helper.updateStudent(req, res, client));
server.post(constants.pathPg.deleteStudent, (req, res) => helper.deleteStudent(req, res, client));
server.post(constants.pathPg.deleteAllStudents, (req, res) => helper.deleteAllStudents(req, res, client));
server.post(constants.pathPg.deleteGroup, (req, res) => helper.deleteGroup(req, res, client));
server.post(constants.pathPg.createGroup, (req, res) => helper.createGroup(req, res, client));
server.post(constants.pathPg.sendImage, (req, res) => helper.sendImage(req, res, client));

server.get(constants.pathPg.getGroups, (req, res) => helper.getGroups(req, res, client));
server.get(constants.pathPg.getStudents, (req, res) => helper.getStudents(req, res, client));
server.get(constants.pathPg.getTeacher, (req, res) => helper.getTeacher(req, res, client));