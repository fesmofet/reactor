export default {

  defaultLocale: 'en',
  defaultIsModal: false,
  buttonRegistration: [
    {
      buttonName: 'buttonName',
      buttonId: 'buttonRegistration',
      buttonClass: 'container-registration__button',
    },
  ],
  buttonAuthorization: [
    {
      buttonAuthName: 'buttonAuthName',
      buttonId: 'buttonAuthLogin',
      buttonClass: 'container-authorization__button',
    },
    {
      buttonAuthName: 'buttonName',
      buttonId: 'buttonAuthRegistration',
      buttonClass: 'container-authorization__button',
    },
  ],
  inputs: [
    {
      labelText: 'login',
      placeholderKey: 'login',
      id: 'loginRegistration',
      type: 'text',
      className: 'container-registration__input',
      inputRef: 'login',
    },
    {
      labelText: 'password',
      placeholderKey: 'password',
      id: 'passwordRegistration',
      type: 'password',
      className: 'container-registration__input',
      inputRef: 'password',
    },
    {
      labelText: 'confirmPassword',
      placeholderKey: 'confirmPassword',
      id: 'confirmPasswordRegistration',
      type: 'password',
      className: 'container-registration__input',
      inputRef: 'passwordAgain',
    },
    {
      labelText: 'email',
      placeholderKey: 'email',
      id: 'emailRegistration',
      type: 'email',
      className: 'container-registration__input',
      inputRef: 'email',
    },
    {
      labelText: 'phone',
      placeholderKey: 'phone',
      id: 'phoneRegistration',
      type: 'phone',
      className: 'container-registration__input',
      inputRef: 'phone',
    },
    {
      labelText: 'keyword',
      placeholderKey: 'keyword',
      id: 'keywordRegistration',
      type: 'text',
      className: 'container-registration__input',
      inputRef: 'keyword',
    },
  ],
  inputsAuth: [
    {
      labelText: 'login',
      placeholderKey: 'login',
      id: 'loginAuthorization',
      type: 'text',
      className: 'container-authorization__input',
      inputRef: 'login',
    },
    {
      labelText: 'password',
      placeholderKey: 'password',
      id: 'passwordAuthorization',
      type: 'password',
      className: 'container-authorization__input',
      inputRef: 'password',
    },
  ],
  inputsAcc:[
  {id:'inputPhoto', type:'file', className:'input-acc'}
  ],
   inputsInfo:[
  {labelText: 'Login', id:'loginAccount', type:'text', className:'data__input', inputRef: 'loginAcc'},
  {labelText: 'E-mail', id:'mailAccount',  type:'email', className:'data__input', inputRef: 'mailAcc'},
  {labelText: 'Phone number', id:'phoneAccount',  type:'text', className:'data__input', inputRef: 'phoneAcc'},
  ],
  options: [
    { value: 'en', resourceKey: 'en' },
    { value: 'ru', resourceKey: 'ru' },
    { value: 'ar', resourceKey: 'ar' },
  ],
  buttonAccount: [
    { buttonName: 'Change' },
    { buttonName: 'Save' },
  ],
  modalsIsOpen: {
    settings: false,
    forgotPassword: false,
    students: false,
  },
  inputsForgottenPassword:[
    {
      labelText: 'login',
      placeholderKey: 'login',
      id: 'loginForgotten',
      type: 'text',
      className: 'container-forgotten__input',
    },
    {
      labelText: 'keyword',
      placeholderKey: 'keyword',
      id: 'keywordForgotten',
      type: 'text',
      className: 'container-forgotten__input',
    },
    {
      labelText: 'getPassword',
      placeholderKey: 'getPassword',
      id: 'passwordRecover',
      type: 'text',
      className: 'container-forgotten__input',
      disabled: true
    },
  ],

  modalStyles: {
    content: {
      width: '30%',
      height: 'auto',
      top: '50%',
      left: '50%',
      right: 'auto',
      padding: '10px',
      bottom: 'auto',
      border: 'none',
      transform: 'translate(-50%, -50%)',
      background: 'linear-gradient(to right top, #eff2f6, #e7eef5, #ddeaf3, #d3e6f0, #c8e3ed)'
    },
  },
};

