import React from "react"

const User = props => {
    const {login} = props;
    return (
    <div className="user-item">
      {login}
    </div>
    );
}

export default React.memo(User)