import React from "react"

const TableHeader = ({
    dictionary
}) => {
    return(
        <div className="admin-table-header">
              <div className="admin-table-header__item">{dictionary.resources.login}</div>
              <div className="admin-table-header__item">{dictionary.resources.activeSessons}</div>
              <div className="admin-table-header__item">{dictionary.resources.popularMode}</div>
              <div className="admin-table-header__item">{dictionary.resources.mostClicked}</div>
              <div className="admin-table-header__item">{dictionary.resources.useChat}</div>
              <div className="admin-table-header__item">{dictionary.resources.device}</div>
              <div className="admin-table-header__item">{dictionary.resources.geolocation}</div>
              <div className="admin-table-header__item">{dictionary.resources.publicIP}</div>
              <div className="admin-table-header__item">{dictionary.resources.browser}</div>
          </div>
    )
}

export default React.memo(TableHeader)