import { connect } from 'react-redux';
import Component from './Paint';

const mapStateToProps = (state) => ({
    dictionary: state.translates.dictionary,
});

const mapDispatchToProps = (dispatch) => ({
});

export default connect(mapStateToProps, mapDispatchToProps)(Component);
