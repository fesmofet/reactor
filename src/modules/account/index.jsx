import { connect } from 'react-redux';
import Component from './Account';
import * as actions from '../../constants/actionCreators';


const mapStateToProps = (state) => ({
  buttons: state.config.buttonAccount,
  inputsAcc: state.config.inputsAcc,
  inputsInfo: state.config.inputsInfo,
  dictionary: state.translates.dictionary,
  teacherData: state.account.teacherData,
  buttonStateAbout: state.account.buttonStateAbout,
});

const mapDispatchToProps = (dispatch) => ({
  logout: () => dispatch(actions.logout()),
  saveAccount: (payload) => dispatch(actions.saveAccount(payload)),
  changeButtonAccount: ()=> dispatch(actions.changeButtonAccount()),
  changePhoto: payload => dispatch(actions.changePhoto(payload))
});

export default connect(mapStateToProps, mapDispatchToProps)(Component);
