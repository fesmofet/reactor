import {connect} from "react-redux";
import Content from './ConvertDates';

const mapStateToProps = state => ({
    dictionary: state.translates.dictionary

});
const mapDispatchToProps = dispatch => ({

});

export default connect(mapStateToProps, mapDispatchToProps)(Content);