import React from 'react';
import "./editableRow.less"
import {  withFormik, Form, Field } from 'formik';
import * as Yup from 'yup';

const EditableRow = ({
     onCancel, errors, touched,  saveText, cancelText, tableIndex, item,
}) => {
        return (
            <Form className="table-row-input-wrapper">
                <span className="row-input-id">{tableIndex}</span>
                <div className="row-input-name">
                    <Field maxLength="50" type="text" name="name"  defaultValue={item.fn} />
                    {touched.name && errors.name && <p className="input-error">{errors.name}</p>}
                </div>
                <div className="row-input-surname">
                    <Field maxLength="50" type="text" name="surname"  defaultValue={item.ln}/>
                    {touched.surname && errors.surname && <p className='input-error'>{errors.surname}</p>}
                </div>
                <div  className="row-input-age">
                    <Field maxLength="50" type="text" name="age"  defaultValue={item.age} />
                    {touched.age && errors.age && <p className='input-error'>{errors.age}</p>}
                </div>
                <div  className="row-input-city">
                    <Field maxLength="50" type="text" name="city"  defaultValue={item.ht} />
                    {touched.city && errors.city && <p className='input-error'>{errors.city}</p>}
                </div>
                <div className="row-input-action">
                    <button type="submit" className="row-input-button">{saveText}</button>
                    <button type="button" onClick={() => onCancel(item.id)}
                            className="row-input-button">{cancelText}
                    </button>
                </div>
            </Form>
        )
    
}
const FormikApp = withFormik({
    handleSubmit(values, { props }) {
        props.onSave(
            props.item.id,
            values.name || props.item.fn,
            values.surname || props.item.ln,
            values.age || props.item.age,
            values.city || props.item.ht,
            )
        props.updateNotification( 
            event, props.item.fn, props.item.ln, props.item.age, props.item.ht,
            values.name, values.surname, values.age, values.city
            )
    },
    validationSchema : props => Yup.object().shape({
        name: Yup.string()
            .matches(/^[a-zA-Zа-яА-Я]+$/, props.dictionary.validation.onlyLetters ),
        surname: Yup.string()
            .matches(/^[a-zA-Zа-яА-Я]+$/, props.dictionary.validation.onlyLetters ),
        age: Yup.string()
            .matches(/^(?![0])/, props.dictionary.validation.ageZero)
            .matches(/^[0-9]+$/, props.dictionary.validation.onlyNumbers)
            .matches(/^[0-9][0-9]?$|^100$/, props.dictionary.validation.age),   
        city: Yup.string()
            .matches(/^[a-zA-Zа-яА-Я]+$/, props.dictionary.validation.onlyLetters ),
    }),
})(EditableRow)

export default React.memo(FormikApp)
