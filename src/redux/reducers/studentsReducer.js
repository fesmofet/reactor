let id = localStorage.getItem('UserID');
const initialState = {
    teacherData: { id, login: "", password: "", email: "", phone: "",  about: null, picture_url: '', keyword: ""},
    tabs: [],
    items: [],
    currentEditItems: [],
    activeTab: 0,
    teacherGroups: [{groupId: '', studentsId: []}],
    renamedTabNumber: 5,
};

export const studentsReducer = (state = initialState, action) => {
    switch(action.type) {
        case 'GET_DATA_TEACHER':
            return {
                ...state,
                teacherData: {
                    ...state.teacherData,
                    id: action.payload.id,
                    login: action.payload.login,
                    password: action.payload.password,
                    email: action.payload.email,
                    phone: action.payload.phone,
                    about: action.payload.about,
                    picture_url: action.payload.picture_url,
                    keyword: action.payload.keyword,
                },
            };
        case 'CHANGE_ACTIVE_TAB':
            return {
                ...state,
                activeTab: parseInt(action.payload),
        };
        case 'DELETE_TAB':
            return {
                ...state,
                activeTab: 0,
            };
        case 'CANCEL_UPDATE':
            return {
                ...state,
                currentEditItems: [...state.currentEditItems.filter((item) => item !== action.payload)],
            };

        case 'UPDATE_TABLE_ROW':
            return {
                ...state,
                currentEditItems: [action.payload, ...state.currentEditItems],
            };
        case 'ADD_TABS':
            return {
                ...state,
                tabs:  action.payload
            };
        case 'ADD_STUDENTS':
            return {
                ...state,
                items:  action.payload
            };

        case 'SET_TAB_AS_RENAMED':
            return {
                ...state,
                renamedTabNumber:  action.payload
            };

        case 'COMPLETE_TAB_RENAME':
            return {
                ...state,
                renamedTabNumber:  action.payload
            };
        default:
            return state;
    }
};