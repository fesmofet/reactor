import { combineReducers } from 'redux';
import { configReducer } from './configReducer';
import { translatesReducer } from './translatesReducer';
import { authorizeReducer } from './authorizeReducer';
import { registrationReducer } from './registrationReducer';
import {modalReducer} from './modalReducer';
import {accountReducer} from './accountReducer';
import {studentsReducer} from "./studentsReducer"
import {reducer as formReducer} from 'redux-form';
import {chatReducer} from "./chatReducer";
import {statisticReducer} from './statisticReducer'

export default combineReducers({
  modals: modalReducer,
  config: configReducer,
  register: registrationReducer,
  authorize: authorizeReducer,
  translates: translatesReducer,
  account: accountReducer,
  form: formReducer,
  students: studentsReducer,
  chatData: chatReducer,
  statistic: statisticReducer,
});
