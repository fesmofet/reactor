import React from 'react';
import ReactDOM from 'react-dom';
import { createStore } from 'redux';
import { Provider } from 'react-redux';
import Layout from './modules/layout';
import rootReducer from './redux/reducers/rootReducer';
import './styles/index.less';
import '../assets/logo.png';
import '../assets/gear.svg';

const store = createStore(rootReducer);

ReactDOM.render(
  <Provider store={store}><Layout /></Provider>,
  document.getElementById('root'),
);
