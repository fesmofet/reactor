import React from 'react';
import PropTypes from 'prop-types';



const InputAuth = (props) => {
  const {
    type, dictionary, placeholderKey, labelText, id, className, refKey,
  } = props;

  return (
    <div className="container-authorization__input">
        <label htmlFor={id}>
          {dictionary.resources[labelText]}
        </label>
        <input
          ref={refKey}
          id={id}
          type={type}
          className={className}
          placeholder={dictionary.placeholders[placeholderKey]}
        />
    </div>
  );
};


InputAuth.propTypes = {
  id: PropTypes.string.isRequired,
  type: PropTypes.string.isRequired,
  dictionary: PropTypes.object.isRequired,
  labelText: PropTypes.string.isRequired,
  className: PropTypes.string.isRequired,
  placeholderKey: PropTypes.string.isRequired,
};

export default InputAuth;
