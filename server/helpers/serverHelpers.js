const logger = require('./logger');


function queryBuilder(arr) {
    let sqlString = "";
    if (arr.fn !== "") {
        sqlString += `fn = '${arr.fn}'`;
    }
    if (arr.ln !== "") {
        if (sqlString === "") {
            sqlString += `ln = '${arr.ln}'`;
        } else {
            sqlString += `, ln = '${arr.ln}'`;
        }
    }
    if (arr.age !== "") {
        if (sqlString === "") {
            sqlString += `age = ${arr.age}`;
        } else {
            sqlString += `, age = ${arr.age}`;
        }
    }
    if (arr.ht !== "") {
        if (sqlString === "") {
            sqlString += `ht = '${arr.ht}'`;
        } else {
            sqlString += `, ht = '${arr.ht}'`;
        }
    }
    sqlString = "UPDATE account SET " + sqlString + " WHERE id = " + arr.id + ";";
    return sqlString;
};

const sendImage = (req, res, client) => {
    let sql = `UPDATE teacher SET picture_url = $1 WHERE id = $2;`;
    client.query(sql, [req.body.img, req.body.id], (err, response) => {
        if (err) {
            res.status(502).send("SERVER ERROR");
        } else {
            res.status(200).send();
        }
    })
};

const forgotPassword = ( req, res, client) => {
    logger.writeLog("ForgotPassword was received");
    let sql = `SELECT password FROM teacher WHERE  login = $1 AND keyword = $2`;
    client.query(sql, [req.body.login, req.body.keyword], (err, response) => {
        if (err) {
            logger.writeLog("ForgotPasswordError: " + err);
            res.status(500).send("SERVER ERROR");
        } else {
            logger.writeLog("ForgotPasswordError: succes");
            res.status(200).json(response.rows);
        }
    });
};

const getNewAvatar =(req, res, client) => {
    logger.writeLog("getNewAvatar was received");
    let sql = `SELECT picture_url FROM teacher WHERE id = $1; `;
    client.query(sql, [req.body.id], (error, response) => {
        if (error) {
            logger.writeLog("getNewAvatar was received" + error);
            res.status(500).send("SERVER ERROR");
        } else {
            if (response.rows.length) {
                logger.writeLog("getNewAvatar: success");
                res.status(200).json(response.rows);
            } else {
                logger.writeLog("getNewAvatar was received" + error);
                res.status(501).send("SERVER ERROR");
            }
        }
    });
};

const authorizeTeacher =(req, res, client) => {
    let sql = `SELECT * FROM teacher WHERE login = $1 AND password = $2 ORDER BY id ASC; `;
    logger.writeLog("AuthTeacher was received");
    client.query(sql, [req.body.login, req.body.password], (error, response) => {
        if (error) {
            logger.writeLog("AuthTeacherError: " + error);
            res.status(500).send("SERVER ERROR");
        } else {
            if (response.rows.length) {
                logger.writeLog("AuthTeacher: success");
                res.status(200).json(response.rows);
            } else {
                logger.writeLog("AuthTeacher: unauthorized");
                res.status(501).send("SERVER ERROR");
            }
        }
    });
};

const createTeacher =(req, res, client) => {
    logger.writeLog("registrationRequest was received");
    let newGroupId = 0;
    let newTeacherId = 0;
    const sql1 = `INSERT INTO teacher (login,password,email,phone,keyword,picture_url) VALUES ($1, $2, $3, $4, $5, $6) RETURNING id, login;`;
    client.query(
      sql1,
      [
        req.body.login,
        req.body.password,
        req.body.email,
        req.body.phone,
        req.body.keyword,
        req.body.avatar
      ],
      (err, response) => {
        if (err) {
          logger.writeLog("registrationRequest: " + err);
          res.status(502).send("SERVER ERROR");
        } else {
          newTeacherId = response.rows[0].id;
          const sql2 = `INSERT INTO course_group (name) VALUES ($1) RETURNING id ;`;
          client.query(sql2, ["Class1"], () => {
            if (err) {
              logger.writeLog("registrationRequest: " + err);
              res.status(502).send("SERVER ERROR");
            } else {
              newGroupId = response.rows[0].id;
              const sql = `INSERT INTO course_teacher (id_teacher , id_group) VALUES ($1, $2);`;
              client.query(sql, [newTeacherId, newGroupId], () => {
                if (err) {
                  logger.writeLog("registrationRequest: " + err);
                  res.status(502).send("SERVER ERROR");
                } else {
                    logger.writeLog("registrationRequest: success");
                    res.status(200).json(response.rows);  
                  
                }
              });
            }
          });
        }
      }
    );
};

const updateTeacher =(req, res, client) => {
    logger.writeLog("updateTeacher was received");
    let sql = `UPDATE teacher SET (login, email, phone, about) = ($2, $3, $4, $5) WHERE id = $1;`;
    client.query(sql, [req.body.id, req.body.login, req.body.email, req.body.phone, req.body.about], (err, response) => {
        if (err) {
            logger.writeLog("updateTeacher was received" + err);
            res.status(502).send("SERVER ERROR");
        } else {
            logger.writeLog("updateTeacher: success");
            res.status(200).send();
        }
    });
}

const saveGroupName = (req, res, client) => {
    logger.writeLog("saveGroupName was received");
    let sql = `UPDATE course_group SET name = $2 WHERE id =$1;`;
    client.query(sql, [req.body.id, req.body.name], (err, response) => {
        if (err) {
            logger.writeLog("saveGroupName was received" + err);
            res.status(500).send("SERVER ERROR");
        } else {
            logger.writeLog("saveGroupName: success");
            res.status(200).send();
        }
    });
};

const createStudent = (req, res, client) => {
    logger.writeLog("createStudent was received");
    let sql = `INSERT INTO account (fn,ln,age,ht,id_group) VALUES ($1, $2,$3, $4, $5) ;`;
    client.query(sql, [req.body.student.fn, req.body.student.ln, req.body.student.age, req.body.student.ht, req.body.student.id_group], (err, response) => {
        if (err) {
            logger.writeLog("createStudent was received" + err);
            res.status(500).send("SERVER ERROR");
        } else {
            logger.writeLog("createStudent: success");
            res.status(200).send();
        }
    });
}
const updateStudent = (req, res, client) => {
    logger.writeLog("updateStudent was received");
    let sql = queryBuilder(req.body);
    client.query(sql, (err) => {
        if (err) {
            logger.writeLog("updateStudent was received" + err);
            res.status(500).send("SERVER ERROR");
        } else {
            logger.writeLog("updateStudent: success");
            res.status(200).send();
        }
    });
};

const deleteStudent = (req, res, client) => {
    logger.writeLog("deleteStudentFromTable was received");
    let sql = `DELETE FROM account WHERE id = $1 `;
    client.query(sql, [req.body.studentId], (err, response) => {
        if (err) {
            logger.writeLog("deleteStudentFromTable: " + err);
            res.status(500).send("SERVER ERROR");
        } else {
            logger.writeLog("deleteStudentFromTable: success");
            res.status(200).send();
        }
    });
};

const deleteAllStudents = (req, res, client) => {
    logger.writeLog("deleteAllStudents was received");
    let sql = `DELETE FROM account WHERE id_group = $1 `;
    client.query(sql, [req.body.groupId], (err, response) => {
        if (err) {
            logger.writeLog("deleteAllStudents was received" + err);
            res.status(500).send("SERVER ERROR");
        } else {
            logger.writeLog("deleteAllStudents: success");
            res.status(200).send();
        }
    });
};

const deleteGroup = (req, res, client) => {
    logger.writeLog("deleteGroup was received");
    let sql = `DELETE FROM course_teacher WHERE id_group = $1 `;
    client.query(sql, [req.body.groupId], (err, response) => {
        if (err) {
            logger.writeLog("deleteGroup was received" + err);
            res.status(501).send("SERVER ERROR");
        } else {
            logger.writeLog("deleteGroup from account");
            let sql2 = `DELETE FROM account WHERE id_group = $1 `;
            client.query(sql2, [req.body.id], (err, response) => {
                if (err) {
                    logger.writeLog("deleteGroup was received" + err);
                    res.status(502).send("SERVER ERROR");
                } else {
                    logger.writeLog("deleteGroup from course_group");
                    let sql3 = `DELETE FROM course_group WHERE id = $1 `;
                    client.query(sql3, [req.body.id], (err, response) => {
                        if (err) {
                            logger.writeLog("deleteGroup was received" + err);
                            res.status(503).send("SERVER ERROR");
                        } else {
                            logger.writeLog("deleteGroup: success");
                            res.status(200).send("OK");
                        }
                    });
                }
            });
        }
    });
};

const createGroup = (req, res, client) => {
    logger.writeLog("createGroup from account");
    let newGroupId = 0;
    let sql2 = `INSERT INTO course_group (name) VALUES ($1) RETURNING id ;`;
    client.query(sql2, ["untitled"], (err, response) => {
        if (err) {
            logger.writeLog("createGroup from account" + err);
            res.status(502).send("SERVER ERROR");
        } else {
            logger.writeLog("createGroup insert into course_teacher");
            newGroupId = response.rows[0].id;
            let sql = `INSERT INTO course_teacher (id_teacher , id_group) VALUES ($1, $2);`;
            client.query(sql, [req.body.id, newGroupId], (err, response) => {
                if (err) {
                    logger.writeLog("createGroup was received" + err);
                    res.status(502).send("SERVER ERROR");
                } else {
                    logger.writeLog("createGroup: success");
                    res.status(200).send();
                }
            });
        }
    });
};

const getGroups = (req, res, client) => {
    logger.writeLog("getGroups was received");
    let sql = `SELECT t1.id_group, t2.name FROM course_teacher t1 inner join course_group t2 on t1.id_group = t2.id WHERE id_teacher = $1 ORDER BY t1.id ASC ;`;
    client.query(sql, [req.query.id_teacher], (err, response) => {
        if (err) {
            logger.writeLog("getGroups was received" + err);
            res.status(500).send("SERVER ERROR");
        } else {
            logger.writeLog("getGroups: success");
            res.status(200).json(response.rows);
        }
    });
};

const getStudents = (req, res, client) => {
    logger.writeLog("getStudents was received");
    let sql = `SELECT id,fn,ln,age,ht FROM account WHERE id_group = $1 ORDER BY id ASC;`;
    client.query(sql, [req.query.id_group], (err, response) => {
        if (err) {
            logger.writeLog("getStudents was received" + err);
            res.status(500).send("SERVER ERROR");
        } else {
            logger.writeLog("getStudents: success");
            res.status(200).json(response.rows);
        }
    });
};

const getTeacher = (req, res, client) => {
    logger.writeLog("getTeacher was received");
    let sql = `SELECT login,password,email,phone,keyword,about,picture_url FROM teacher WHERE id = $1 ORDER BY id ASC;`;
    client.query(sql, [req.query.id], (err, response) => {
        if (err) {
            logger.writeLog("getTeacher was received" + err);
            res.status(500).send("SERVER ERROR");
        } else {
            logger.writeLog("getTeacher: success");
            res.status(200).json(response.rows);
        }
    });
};

const cors = (req, res, next) => {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
};

module.exports = {
    queryBuilder: queryBuilder,
    sendImage,
    forgotPassword,
    getNewAvatar,
    authorizeTeacher,
    createTeacher,
    updateTeacher,
    saveGroupName,
    createStudent,
    updateStudent,
    deleteStudent,
    deleteAllStudents,
    deleteGroup,
    createGroup,
    getGroups,
    getStudents,
    getTeacher,
    cors,
};