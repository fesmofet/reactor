import React from 'react';
import PropTypes from 'prop-types';

const Select = (props) => {
  const {
    callback, options, className, dictionary,
  } = props;
  const checkSelected = () => {
    return localStorage.getItem("locale");
  };
  return (
    <select className={className} onChange={callback && callback} defaultValue={checkSelected()}>
      {options.map((option) => <option key={option.value} value={option.value}>{dictionary.langOptions[option.resourceKey]}</option>)}
    </select>
  );
};

Select.propTypes = {
  options: PropTypes.array,
  callback: PropTypes.func,
  className: PropTypes.string,
};

Select.defaultProps = {
  options: [],
  callback: null,
  className: 'select-wrapper',
};

export default React.memo(Select);
