const express = require('express')
const mongoose =require('mongoose')
const User = require('./models/user')
const bodyParser = require('body-parser');

const app = express()

app.use(function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));
mongoose.set('useFindAndModify', false);

const PORT = 5678

async function start() {
    try {
      const url =`mongodb+srv://boris:Dlj8lw0cZep4FBe5@cluster0-hnwd6.mongodb.net/test?retryWrites=true&w=majority`
      await mongoose.connect(url, {useUnifiedTopology: true, useNewUrlParser: true})
      app.listen(PORT, () => {
        console.log(`Server is running on port ${PORT}`)
    })
    } catch(e) {
      console.log(e) 
    }
    
  }
  start()

app.post('/', async (req, res) => {
    const user =  new User({
      _id: req.body._id,
      login: req.body.login,
    })
    try {
      await user.save()
    } catch(e) {
      console.log(e)
    }
})

app.post('/add-session', async (req, res) => {
  
  const id = parseInt(req.body._id)
  User.findOneAndUpdate({_id: id}, {
    '$inc' :  {activeSessions: 1},
    browser: req.body.browser,
    publicIP: req.body.publicIP,
    device: req.body.device,
    }, function(err, result) {
    if (err) {
      console.log(err)
    } else {
      res.send(result);
    }
  })
})
app.post('/add-geo', async (req, res) => {
  const id = parseInt(req.body._id)
  User.findOneAndUpdate({_id: id}, {
    geo: req.body.geo
    }, function(err, result) {
    if (err) {
      console.log(err)
    } else {
      res.send(result);
    }
  })
})
//Timers
app.post('/students-timer', async (req, res) => {
  const id = parseInt(req.body._id)
  User.findOneAndUpdate({_id: id}, {
    '$inc' :  {timeStudents: req.body.timeStudents},
    }, function(err, result) {
    if (err) {
      console.log(err)
    } else {
      res.send(result);
    }
  })
})

app.post('/paint-timer', async (req, res) => {
  const id = parseInt(req.body._id)
  User.findOneAndUpdate({_id: id}, {
    '$inc' :  {timePaint: req.body.timePaint},
    }, function(err, result) {
    if (err) {
      console.log(err)
    } else {
      res.send(result);
    }
  })
})

app.post('/calc-timer', async (req, res) => {
  const id = parseInt(req.body._id)
  User.findOneAndUpdate({_id: id}, {
    '$inc' :  {timeCalculator: req.body.timeCalculator},
    }, function(err, result) {
    if (err) {
      console.log(err)
    } else {
      res.send(result);
    }
  })
})

app.post('/account-timer', async (req, res) => {
  const id = parseInt(req.body._id)
  User.findOneAndUpdate({_id: id}, {
    '$inc' :  {timeAccount: req.body.timeAccount},
    }, function(err, result) {
    if (err) {
      console.log(err)
    } else {
      res.send(result);
    }
  })
})

app.post('/currency-timer', async (req, res) => {
  const id = parseInt(req.body._id)
  User.findOneAndUpdate({_id: id}, {
    '$inc' :  {timeCurrency: req.body.timeCurrency},
    }, function(err, result) {
    if (err) {
      console.log(err)
    } else {
      res.send(result);
    }
  })
})

app.post('/converter-timer', async (req, res) => {
  const id = parseInt(req.body._id)
  User.findOneAndUpdate({_id: id}, {
    '$inc' :  {timeConverter: req.body.timeConverter},
    }, function(err, result) {
    if (err) {
      console.log(err)
    } else {
      res.send(result);
    }
  })
})
// Click buttons
app.post('/click-create', async (req, res) => {
  const id = parseInt(req.body._id)
  User.findOneAndUpdate({_id: id}, {
    '$inc' :  {createClick: 1},
    }, function(err, result) {
    if (err) {
      console.log(err)
    } else {
      res.send(result);
    }
  })
})
app.post('/click-update', async (req, res) => {
  const id = parseInt(req.body._id)
  User.findOneAndUpdate({_id: id}, {
    '$inc' :  {updateClick: 1},
    }, function(err, result) {
    if (err) {
      console.log(err)
    } else {
      res.send(result);
    }
  })
})
app.post('/click-delete', async (req, res) => {
  const id = parseInt(req.body._id)
  User.findOneAndUpdate({_id: id}, {
    '$inc' :  {deleteClick: 1},
    }, function(err, result) {
    if (err) {
      console.log(err)
    } else {
      res.send(result);
    }
  })
})
app.post('/click-clear', async (req, res) => {
  const id = parseInt(req.body._id)
  User.findOneAndUpdate({_id: id}, {
    '$inc' :  {clearClick: 1},
    }, function(err, result) {
    if (err) {
      console.log(err)
    } else {
      res.send(result);
    }
  })
})
app.post('/click-message', async (req, res) => {
  const id = parseInt(req.body._id)
  User.findOneAndUpdate({_id: id}, {
    '$inc' :  {chatMessages: 1},
    }, function(err, result) {
    if (err) {
      console.log(err)
    } else {
      res.send(result);
    }
  })
})

//GET all info
app.get('/render', async (req, res) => {
  User.find()
  .then(users => res.send(users))
})

app.post('/update-login', async (req, res) => {
  
  const id = parseInt(req.body._id)
  User.findOneAndUpdate({_id: id}, {
    login: req.body.login,
    }, function(err, result) {
    if (err) {
      console.log(err)
    } else {
      res.send(result);
    }
  })
})