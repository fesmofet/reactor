import React from "react"
import "./tableCaption.less"
const TableCaption = (props) => {
    const {dictionary} = props;
    return(
        <div className="table-caption-wrapper">
            <span className="caption-id">№</span>
            <span className="caption-name">{dictionary.resources.name}</span>
            <span className="caption-surname">{dictionary.resources.surname}</span>
            <span className="caption-age">{dictionary.resources.age}</span>
            <span className="caption-city">{dictionary.resources.city}</span>
            <span className="caption-actions">{dictionary.resources.action}</span>

        </div>
    )
}
export default React.memo(TableCaption)