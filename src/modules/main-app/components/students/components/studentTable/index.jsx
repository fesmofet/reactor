import StudentsTable from "./StudentsTable.jsx";
import {connect} from "react-redux";
import * as actions from "../../../../../../constants/actionCreators";


const mapStateToProps = state => ({
    teacherData: state.students.teacherData,
    teacherGroups: state.students.teacherGroups,
    dictionary: state.translates.dictionary,
    tabs: state.students.tabs,
    items: state.students.items,
    currentEditItems: state.students.currentEditItems,
    activeTab: state.students.activeTab,
    renamedTabNumber: state.students.renamedTabNumber,
});

const mapDispatchToProps = dispatch => ({
    changeActiveTab: payload => dispatch(actions.changeActiveTab(payload)),
    deleteTab: payload => dispatch(actions.deleteTab(payload)),
    cancelUpdate: payload => dispatch(actions.cancelUpdate(payload)),
    updateTableRow: payload => dispatch(actions.updateTableRow(payload)),
    addTabs: payload => dispatch(actions.addTabs(payload)),
    addStudents: payload => dispatch(actions.addStudents(payload)),
    setTabAsRenamed: payload => dispatch(actions.setTabAsRenamed(payload)),
    completeTabRenamed: payload => dispatch(actions.completeTabRenamed(payload)),
 
});

export default connect(mapStateToProps, mapDispatchToProps)(StudentsTable);