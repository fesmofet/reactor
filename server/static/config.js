const constants = require('./constants');

module.exports = {
  connectionString: constants.connectionString,
};
