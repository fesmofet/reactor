import React from 'react';
import PropTypes from 'prop-types';

const ControlButtons = (props) => {
  const { className, buttons, buttonName } = props;

  return (
    <div className={className}>
      {buttons.map((button) => (
        <button
          key={button.id}
          onClick={button.callback}
          className={button.className}
          value={buttonName}
        />
       
      ))}
    </div>
  );
};

ControlButtons.propTypes = {
  buttons: PropTypes.array.isRequired,
  className: PropTypes.string.isRequired,
};

export default React.memo(ControlButtons);
