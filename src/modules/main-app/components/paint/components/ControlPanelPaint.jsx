import React from 'react';
import PropTypes from "prop-types";

const ControlPanelPaint = props => {
    const {dictionary} = props;
    return(
        <div className="settings-wrapper">
            <div className="control-wrap">
                <div>
                    <label htmlFor={'lineWidth'}>{dictionary.resources.lineWidth}</label>
                    <input type="range" min="10" max="20" defaultValue="10"
                                          id="lineWidth"/>
                    <label htmlFor={'figureSize'}>{dictionary.resources.figureSize}</label>
                    <input type="range" min="25" max="70" defaultValue="10"
                                          id="figureSize"/>
                </div>
                <div>
                    <label htmlFor={'color'}>{dictionary.resources.pickColor}</label>
                    <input id="color" type="color"/>
                </div>
                <div className='div-block-button'>
                    <button className="custom-button" id="pencil"  />
                    <button className="custom-button" id="square"/>
                    <button className="custom-button" id="rectangle"/>
                    <button className="custom-button" id="triangle"/>
                    <button className="custom-button" id="circle"/>
                    <button className="custom-button" id="clear">{dictionary.resources.clear}</button>
                </div>
            </div>

        </div>
    )
};

ControlPanelPaint.propTypes = {
    dictionary: PropTypes.object.isRequired,
};

export default React.memo(ControlPanelPaint);