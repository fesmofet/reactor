import config from '@config/config';

const initialState = {
  settings: {
    type: 'settings',
    data: null,
    isOpen: config.modalsIsOpen.settings,
  },
  forgotPassword: {
    type: 'forgotPassword',
    data: null,
    isOpen: config.modalsIsOpen.forgotPassword,
  },
  students: {
    type: 'students',
    data: null,
    isOpen: config.modalsIsOpen.students,
  },
  notificationOff: false, 
};

export const modalReducer = (state = initialState, action) => {
  switch (action.type) {
    case 'TOGGLE_MODAL_WINDOW':
      return {
        ...state,
        [action.payload.type]: {
          ...state[action.payload.type],
          data: action.payload.data,
          isOpen: !state[action.payload.type].isOpen,
        },
      };
    case 'TOGGLE_NOTIFICATION':
      return {
        ...state,
        notificationOff: action.payload
      };
    default:
      return state;
  }
};
