import { connect } from 'react-redux';
import Component from './ModalStudents';
import * as action  from "@constants/actionCreators";

const mapStateToProps = (state) => ({
  options: state.config.options,
  dictionary: state.translates.dictionary,
  customStyles: state.config.modalStyles,
  defaultLocale: state.translates.locale,
  modals: state.modals,
  tabs: state.students.tabs,
  id: state.students.teacherData.id,
});
const mapDispatchToProps = (dispatch) => ({
  changeLocale: (payload) => dispatch({ type: 'CHANGE_LOCALE', payload }),
  toggleModalWindow: (payload) => dispatch({ type: 'TOGGLE_MODAL_WINDOW', payload }),
  toggleNotification: payload => dispatch(action.toggleNotification(payload)),
});

export default connect(mapStateToProps, mapDispatchToProps)(Component);
