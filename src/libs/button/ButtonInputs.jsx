import React from "react";

export default class ButtonInputs extends React.Component {

    consoleChangeClick = () => (this.props.changeButtonInput(), console.log("CHAAAANGEEE!!!!!___"));
    consoleSaveClick = () => (this.props.changeButtonInput(), console.log("SAAAAAVEEEE!!!!!___"));

    renderSwitchButton = (state) => {
        return state ? <button onClick={this.consoleChangeClick}>Change</button> : <button onClick={this.consoleSaveClick}>Save</button>;
    };

    render() {
        return (
            <div className='change-avatar'>
                {this.renderSwitchButton(this.props.buttonStateInput)}
            </div>
        )
    }
}


