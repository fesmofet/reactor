export default {
  resources: {
    reset: 'Сброс настроек',
    offNotification: 'Выключить уведомления',
    calc: 'Инженерный калькулятор',
    converterL: 'Конвертер велечин',
    сonverterM: 'Конвертер валют',
    converter: "Конвертер",
    paint: 'Рисовалка',
    mode: 'Режим',
    activeSessons: 'Активных сессий',
    popularMode: 'Популярный режим',
    mostClicked: 'Больше кликов по',
    useChat: 'Использование чата',
    device: 'Device',
    geolocation: 'Геопозиция',
    publicIP: 'Public IP',
    browser: 'Браузер',
    send: 'Отправить',
    students: 'Студенты',
    action: 'Действие',
    create: 'Создать',
    clear: 'Очистить',
    age: 'Возраст',
    city: 'Город',
    name: 'Имя',
    surname: 'Фамилия',
    login: 'Логин',
    phone: 'Телефон',
    password: 'Пароль',
    settings: 'Настройки',
    buttonAuthName: 'Логин',
    buttonName: 'Регистрация',
    keyword: 'Ключевое слово',
    email: 'Электронная почта',
    registration: 'Регистрация',
    authorization: 'Авторизация',
    confirmPassword: 'Подтвердите пароль',
    buttonAuthRegistration: 'Регистрация',
    modalSettingsMainText: 'Выберите язык',
    forgottenPassword: 'Восстановить пароль',
    forgottenError: 'Неверный логин или ключевое слово',
    getPassword: 'Получить пароль',
    meter: "Метр",
    verst: "Верста",
    mile: "Миля",
    foot: "Фут",
    yard: "Ярд",
    converterLength: 'Конвертер длины',
    convert: 'Конвертировать',
    btnChange: 'Изменить',
    btnSave: 'Сохранить',
    pickColor: 'Выберите цвет',
    lineWidth: 'Выберите толщину линии',
    figureSize: 'Выберите размер фигуры'
  },
  dialogs: {
    backToAuth: 'Вернуться на авторизацию',
    forgotPassword: 'Если вы забыли пароль',
    controlPanel: 'Контрольная панель',
    addStudent: 'Добавить студента',
    update: 'обновить',
    delete: 'удалить',
    save: 'сохранить',
    cancel: 'отмена',
    emptyString: 'Значение не может быть пустой строкой',
    deleteTab: 'Удаление таблицы приведет к потере данных. Восстановить их будет невозможно. Вы уверены, что хотите удалить таблицу?',
    clearTable: 'Очистка таблицы приведет к потере данных. Восстановить их будет невозможно. Вы уверены, что хотите удалить таблицу?',
    reset: 'Сброс приведет к потере данных. Восстановить их будет невозможно. Вы уверены, что хотите сбросить всё?',
    createStudent: 'Вы создали студента',
    deleteStudent: 'Вы удалили студента',
    updateStudent: 'Вы обновили информацию с',
    to: 'на',
  },
  placeholders: {
    age: 'возраст студента...',
    city: 'родной город...',
    name: 'имя студента...',
    surname: 'фамилия студента...',
    phone: '+38XXXXXXXXXX',
    login: 'Введите логин здесь...',
    email: 'Введите почту здесь...',
    password: 'Введите свой пароль...',
    keyword: 'Напишите своё ключевое слово',
    confirmPassword: 'Подтвердите свой пароль...',
    converterMoney: 'Введите сумму...',
    converterLength: "Введите число...",
  },
  validation: {
    required: 'Это поле обязательно',
    onlyNumbers: 'Поле только для цифр!',
    age: 'Максимальный возраст 100 лет',
    ageZero: 'Возраст не может быть 0',
    onlyLetters: 'Буквы A-Z А-Я',
    cannotStartNumber: 'Логин не может начинаться с цифр',
    onlyLettersAndNumbers: 'Логин может содержать только латинские буквы и цифры',
    minLengthError: 'Логин должен быть не менее 5 символов',
    maxLengthError30: 'Логин не должен быть более 30 символов',
    maxLengthError15: 'Логин не должен быть более 15 символов',
    maxLengthError13: 'Телефон должен быть не менее 13 символов',
    passwordMatch: 'Пароли не совпадают',
    emailError: 'email должен быть валидным',
    phoneError: 'Коректный формат телефона +380000000000',
  },
  langOptions: {
    en: 'англ',
    ru: 'русс',
    ar: 'араб',
  },
};
