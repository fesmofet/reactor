import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { authorizationRequest } from '@/utils/requests';
import './authorization.less';
import InputAuth from './components/Input';
import ButtonAuthorization from './components/Buttons';
import Header from '../header/index';

export default class Authorization extends Component {
  ref = {
    login: React.createRef(),
    password: React.createRef(),
  };
  componentDidMount() {
    localStorage.clear()
  }
  requestHandler = () => {
    const { login, password } = this.ref;
    authorizationRequest(
      login.current.value,
      password.current.value,
      this.props.login,
      this.props.history,
      this.props.getDataTeacherAuthorization,
    );
  };

  toggleForgotModalWindow = () => {
    const { toggleModalWindow } = this.props;
    toggleModalWindow({ type: 'forgotPassword', data: null });
  };

  render() {
    const { inputsAuth, buttons, dictionary } = this.props;
    return (
      <div className="wrapper-authorization">

        <Header />

        <div className="container-authorization">
          <div>
            <span>{dictionary.resources.authorization}</span>
          </div>
          {inputsAuth.map((input) => (
            <InputAuth
              id={input.id}
              key={input.id}
              type={input.type}
              labelText={input.labelText}
              className={input.className}
              refKey={this.ref[input.inputRef]}
              dictionary={dictionary}
              placeholderKey={input.placeholderKey}
            />
          ))}
          {buttons.map((elem) => (
            <ButtonAuthorization
              id={elem.buttonId}
              key={elem.buttonId}
              dictionary={dictionary}
              buttonClass={elem.buttonClass}
              buttonName={elem.buttonAuthName}
              onClick={
                elem.buttonAuthName !== 'buttonAuthName'
                  ? () => {
                    this.props.history.push('/registration');
                  }
                  : this.requestHandler
              }
            />
          ))}
          <div className="forgotten-password">
            <span onClick={this.toggleForgotModalWindow}>
              {dictionary.dialogs.forgotPassword}
            </span>
          </div>
        </div>
      </div>
    );
  }
}
Authorization.propTypes = {
  inputsAuth: PropTypes.array.isRequired,
  buttons: PropTypes.array.isRequired,
  dictionary: PropTypes.object.isRequired,
};
