import actionTypes from '@constants/actionTypes';

const id = localStorage.getItem('UserID');

const initialState = {
  buttonStateAbout: true,
  teacherData: {picture_url: ''},
};

export const accountReducer = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.SAVE_ACCOUNT_INFO:
      return {
        ...state,
        teacherData: action.payload,
      };
    case actionTypes.CHANGE_BUTTON_ABOUT:
      return {
        ...state,
        buttonStateAbout: !state.buttonStateAbout,
      };
    case actionTypes.CHANGE_PHOTO:
      return {
        ...state,
        teacherData: {
          ...state.teacherData,
          picture_url: action.payload.picture_url
        }
      };
    default:
      return state;
  }
};
