import React, { Component } from "react";
import { Route, Switch } from "react-router-dom";
import Paint from "../main-app/components/paint/Paint"
import Students from "../main-app/components/students/index"
import Calculator from "../main-app/components/calculator/Calculator";
import LengthConverter from "../main-app/components/lengthConverter/index";
import MoneyConverter from "../main-app/components/moneyConverter/index";
import Account from "../account/index"
import "./mainApp.less"
import path from "@constants/path.js"
import {SocketConnection} from "./socketConnection"
import axios from 'axios';
import {nameOfBrowser, getGeo, mobileCheck} from "../../utils/utils"

export default class MainApp extends Component {
  constructor(props) {
    super(props)
    new SocketConnection(path.ws);
    SocketConnection.getSocket().onopen = () => {
      const login = localStorage.getItem("UserLogin")
      SocketConnection.getSocket().send(JSON.stringify({
           id: localStorage.getItem("UserID"),
           sid: new Date().getTime().toString(),
           login: login,
           command: "WS_SAVE_DATA",
        }));
      
    };  
    SocketConnection.getSocket().onmessage = response => {
      const serverData = JSON.parse(response.data);
      props.addUser(serverData)  
    };
    SocketConnection.getSocket().onclose = () => {
      console.log('bye')
      SocketConnection.getSocket().send(JSON.stringify({
        login: localStorage.getItem("UserLogin"),
        command: "USER_LOGOUT",
     }));
    }
  }

  componentDidMount() {
    const id = localStorage.getItem("UserID");
    const browser = nameOfBrowser();  
    const device = mobileCheck()
    getGeo()
    axios.get(`https://api.ipify.org`).then(function (response) {
      axios.post(`${path.mongo}add-session`, {
        _id: id,
        browser: browser,
        publicIP: response.data,
        device: device
      })
    });


  }

  render() {

    const { history, dictionary } = this.props;
   

    return (
      <div className="main-wrapper">
        <Switch>
          <Route path="/main/paint" >
            <Paint history={history}
            dictionary={dictionary}/>
          </Route>
          <Route path="/main/students" >
            <Students history={history}/>
          </Route>
          <Route path="/main/calculator">
            <Calculator history={history}/>
          </Route>
          <Route path="/main/length-converter" >
            <LengthConverter history={history}/>
          </Route>
          <Route path="/main/money-converter" >
            <MoneyConverter history={history}/>
          </Route>
          <Route path="/main/account" >
            <Account history={history}/>
          </Route>
        </Switch>
      </div>
    )
  }
}

