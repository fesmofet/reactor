import React from "react"

const Message = props => {
    const {text, login} = props;
    const ourLogin = localStorage.getItem('UserLogin')
    return(
        <div className={login === ourLogin ? "message-item" : "not-our-message"}>
      <span>{`${login}:`}</span>
      <span>{text}</span>
      
    </div>
    );
}

export default React.memo(Message)