import { connect } from 'react-redux';
import Component from './AccountHeader';
import * as actions from '@constants/actionCreators';

const mapStateToProps = (state) => ({
  inputs: state.config.inputs,
  dictionary: state.translates.dictionary,
  selectValues: state.config.selectValues,
});

const mapDispatchToProps = (dispatch) => ({
  logout: () => dispatch(actions.logout()),
  toggleModalWindow: payload => dispatch(actions.toggleModalWindow(payload)),
});

export default connect(mapStateToProps, mapDispatchToProps)(Component);
