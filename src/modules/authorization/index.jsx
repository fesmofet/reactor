import { connect } from 'react-redux';
import Component from './Authorization';
import actionTypes from '../../constants/actionTypes';
import * as actions from '@constants/actionCreators';

const mapStateToProps = (state) => ({
  isLoggedIn: state.authorize.isLoggedIn,
  inputsAuth: state.config.inputsAuth,
  buttons: state.config.buttonAuthorization,
  dictionary: state.translates.dictionary,
});

const mapDispatchToProps = (dispatch) => ({
  login: () => dispatch(actions.login()),
  logout: () => dispatch(actions.logout()),
  // CHANGE_LOCALE: (payload) => dispatch({
  //   type: actionTypes.CHANGE_LOCALE,
  //   payload,
  // }), ЧТО ЭТО??? FIXME
  toggleModalWindow: (payload) => dispatch(actions.toggleModalWindow(payload)),
  getDataTeacherAuthorization:  payload => dispatch(actions.getDataTeacherAuthorization(payload)),
});

export default connect(mapStateToProps, mapDispatchToProps)(Component);
