import React, { Component } from 'react';
import "./admin.less"
import TableHeader from "./components/TableHeader"
import TableRow from "./components/TableRow"
import axios from 'axios';
import path from '@constants/path.js';
import {getStatistic} from '@/utils/requests'

export default class Admin extends Component {
  componentDidMount() {
    getStatistic(this.props.saveStatistic)
  }

  render() {
    const {users} = this.props
    return (
      <div className="admin-wrapper">
        <div>
          <h1>Admin</h1>
          <button onClick={this.props.logout}>Logout</button>
        </div>
        <div className="admin-table-wrapper">
          <TableHeader dictionary={this.props.dictionary}/>
          <div className="row-scroll">
            {users.map(user => {
              return <TableRow 
                key={user._id} 
                login={user.login}
                activeSessions={user.activeSessions}
                browser={user.browser}
                ip={user.publicIP} 
                geo={user.geo}
                tStudents={user.timeStudents}
                tPaint={user.timePaint}
                tCalc={user.timeCalculator}
                tAccount={user.timeAccount}
                tCurrency={user.timeCurrency}
                tConverter={user.timeConverter}
                device={user.device}
                chat={user.chatMessages}
                createBtn={user.createClick}
                clearBtn={user.clearClick}
                deleteBtn={user.deleteClick}
                updateBtn={user.updateClick}
              />
            })}
          </div>
        </div>
      </div>
    );
  }
}
