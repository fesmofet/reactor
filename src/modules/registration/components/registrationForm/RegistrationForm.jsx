import React from 'react';
import { withFormik, Form, Field } from 'formik';
import * as Yup from 'yup';
import { registrationRequest } from '@/utils/requests';
import './validate.less';

const RegistrationForm = ({
  values, handleChange, errors, touched, dictionary,
}) => (
  <Form className="registration-form">
    <div className="container-registration__input">
      <label htmlFor="regLogin">{dictionary.resources.login}</label>
      <Field id="regLogin" type="text" name="login" placeholder={dictionary.placeholders.login} />
      {touched.login && errors.login && <p className="validation-error">{errors.login}</p>}
    </div>
    <div className="container-registration__input">
      <label htmlFor="regPass">{dictionary.resources.password}</label>
      <Field id="regPass" type="password" name="password" placeholder={dictionary.placeholders.password} />
      {touched.password && errors.password && <p className="validation-error">{errors.password}</p>}
    </div>
    <div className="container-registration__input">
      <label htmlFor="regCPass">{dictionary.resources.confirmPassword}</label>
      <Field id="regCPass" type="password" name="cPassword" placeholder={dictionary.placeholders.confirmPassword} />
      {touched.cPassword && errors.cPassword && <p className="validation-error">{errors.cPassword}</p>}
    </div>
    <div className="container-registration__input">
      <label htmlFor="regEmail">{dictionary.resources.email}</label>
      <Field id="regEmail" type="text" name="email" placeholder={dictionary.placeholders.email} />
      {touched.email && errors.email && <p className="validation-error">{errors.email}</p>}
    </div>
    <div className="container-registration__input">
      <label htmlFor="regPhone">{dictionary.resources.phone}</label>
      <Field id="regPhone" type="text" name="phone" placeholder={dictionary.placeholders.phone} maxLength="13" autoComplete="off" />
      {touched.phone && errors.phone && <p className="validation-error">{errors.phone}</p>}
    </div>
    <div className="container-registration__input">
      <label htmlFor="regKeyword">{dictionary.resources.keyword}</label>
      <Field id="regKeyword" type="text" name="keyword" placeholder={dictionary.placeholders.keyword} />
      {touched.keyword && errors.keyword && <p className="validation-error">{errors.keyword}</p>}
    </div>
    <div className="container-registration__button">
      <button type="submit">{dictionary.resources.buttonName}</button>
    </div>

  </Form>
);
const FormikApp = withFormik({
  mapPropsToValues({
    login, email, password, phone, keyword, cPassword,
  }) {
    return {
      login: login || '',
      password: password || '',
      email: email || '',
      phone: phone || '',
      keyword: keyword || '',
      cPassword: keyword || '',
    };
  },
  handleSubmit(values, { props }) {
    registrationRequest(values, props.history);
  },
  validationSchema: (props) => Yup.object().shape({
    login: Yup.string()
      .matches(/^(?![0-9])/, props.dictionary.validation.cannotStartNumber)
      .matches(/^[a-zA-Z0-9]+$/, props.dictionary.validation.onlyLettersAndNumbers)
      .min((5), props.dictionary.validation.minLengthError)
      .max((30), props.dictionary.validation.maxLengthError30)
      .required(props.dictionary.validation.required),
    password: Yup.string()
      .min((5), props.dictionary.validation.minLengthError)
      .max((15), props.dictionary.validation.maxLengthError15)
      .required(props.dictionary.validation.required),
    cPassword: Yup.mixed().test('match', props.dictionary.validation.passwordMatch, function (password) {
      return password === this.parent.password;
    }).required(props.dictionary.validation.required),
    email: Yup.string()
      .matches(/^[-._a-z0-9]+@(?:[a-z0-9][-a-z0-9]+\.)+[a-z]{2,6}$/, props.dictionary.validation.emailError)
      .required(props.dictionary.validation.required),
    phone: Yup.string()
      .matches(/^\+380\d{3}\d{2}\d{2}\d{2}$/, props.dictionary.validation.phoneError)
      .min((13), props.dictionary.validation.maxLengthError13),
    keyword: Yup.string(),

  }),
})(RegistrationForm);

export default React.memo(FormikApp);
