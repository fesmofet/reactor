import React from 'react';
import PropTypes from 'prop-types';
import Modal from 'react-modal';
import Select from '@libs/select/Select';
import * as utils from '@utils/utils';
import './ModalSettings.less';

export default class SettingsModal extends React.Component {
  static propTypes = {
    options: PropTypes.array.isRequired,
    dictionary: PropTypes.object.isRequired,
    changeLocale: PropTypes.func.isRequired,
    toggleModalWindow: PropTypes.func.isRequired,
  };

  onChange = (event) => {
    const { changeLocale } = this.props;
    const selectedLocale = event.currentTarget.value;
    utils.setBodyDirection(selectedLocale === 'ar' ? 'rtl' : 'ltr');
    utils.setDataLS('locale', selectedLocale);
    changeLocale(selectedLocale);
  };

  toggleSettingsWindow = () => {
    const { toggleModalWindow } = this.props;
    toggleModalWindow({ type: 'settings', data: null });
  };

  render() {
    return (
      <Modal
        isOpen={this.props.isOpen}
        style={this.props.customStyles}
        onRequestClose={this.toggleSettingsWindow}
        ariaHideApp={false}
      >
        <div className="settings-modal-wrapper">
          <header className="settings-modal-wrapper__header-modal">
            <span>{this.props.dictionary.resources.settings}</span>
            <button className="custom-button" onClick={this.toggleSettingsWindow}>x</button>
          </header>
          <div className="settings-modal-wrapper__content-modal">
            <Select className="" dictionary={this.props.dictionary} options={this.props.options} callback={this.onChange} />
          </div>
        </div>
      </Modal>
    );
  }
}
