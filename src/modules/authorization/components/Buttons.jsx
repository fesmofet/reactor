import React from 'react';
import PropTypes from 'prop-types';

const ButtonAuthorization = (props) => {
  const {
    dictionary, id, buttonClass, onClick, buttonName,
  } = props;
  return (
    <div className="container-authorization__button">
      <button id={id} className={buttonClass} onClick={onClick}>
        {dictionary.resources[buttonName]}
      </button>
    </div>
  );
};

ButtonAuthorization.propTypes = {
  id: PropTypes.string.isRequired,
  dictionary: PropTypes.object.isRequired,
  buttonName: PropTypes.string.isRequired,
  buttonClass: PropTypes.string.isRequired,
  onClick: PropTypes.func.isRequired,
};

export default ButtonAuthorization;
