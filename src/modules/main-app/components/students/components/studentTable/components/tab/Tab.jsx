import React from 'react';
import classNames from "classnames";
import "./tab.less"

export default class Tab extends React.Component {

    render() {

        const {labelClass, saveTabName, isTabRenamed, groupId, name, isActive, sequenceNumber, changeActiveTab, onDeleteTab, cantBeDeleted, renameTab} = this.props;
        const tabClassName = classNames('tab-wrapper', {"tab-wrapper--active": isActive});
        const tabInputClassName = classNames('tab-wrapper__input', {"tab-wrapper__input--mode": isActive});
        return (
            <div onClick={changeActiveTab}
                 accessKey={sequenceNumber}
                 className={tabClassName}
                 onDoubleClick={renameTab}>
                {isTabRenamed ?
                    <input maxLength="20" className={tabInputClassName}
                           onBlur={(e) => saveTabName(groupId, e)}
                           defaultValue={name} type="text"/>
                    :
                    <label className={labelClass}>{name}</label>
                }
                {cantBeDeleted ? <button onClick={(event) => onDeleteTab(event, groupId)}
                                         className="tab-wrapper__close-button">x</button> : ""}
            </div>
        )
    }
};

