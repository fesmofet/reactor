import React from "react";
import PropTypes from "prop-types";
import Component from "../modules/account/Account";

export default class StateTwo extends React.Component{


    render() {
        const {
            labelText, id
        } = this.props;
        return (
            <div className="data">
                <label htmlFor={id}>
                    {[labelText]}
                    <div className="flex3">
                        <span>My login</span>
                        <span>My e-mail</span>
                        <span>My phone</span>
                    </div>
                </label>
            </div>
        )
    }

};
