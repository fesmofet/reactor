import React from 'react';
import {  withFormik, Form, Field } from 'formik';
import * as Yup from 'yup';
import './AccountForm.less';

const AccountForm = ({ errors, touched, teacherData, dictionary, handleChange, changeButtonAccount}) => {
    return (
        <Form className="my-account-form" >

            <label htmlFor="myAccountLogin">{dictionary.resources.login}</label>
            <div className="my-account-form__input">
                <input id='myAccountLogin' maxLength="20" type="text" name="login" defaultValue={ teacherData.login} onChange={handleChange}/>
                {touched.login && errors.login && <p className="input-error">{errors.login}</p>}
            </div>
            <div className="my-account-form__input">
                <label htmlFor="myAccountEmail">{dictionary.resources.email}</label>
                <input id='myAccountEmail'  maxLength="20" type="text" name="email"  defaultValue={ teacherData.email} onChange={handleChange}/>
                {touched.email && errors.email && <p className='input-error'>{errors.email}</p>}
            </div>
            <div  className="my-account-form__input">
                <label htmlFor="myAccountPhone">{dictionary.resources.phone}</label>
                <input id='myAccountPhone' maxLength="20" type="text" name="phone"  defaultValue={ teacherData.phone } onChange={handleChange}/>
                {touched.phone && errors.phone && <p className='input-error'>{errors.phone}</p>}
            </div>
            <div  className="my-account-form__about">
                <label htmlFor="myAccountAboutMe">{dictionary.resources.about}</label>
                <textarea id='myAccountAboutMe' maxLength="500" name="about"  defaultValue={ teacherData.about } onChange={handleChange}/>
                {touched.about && errors.about && <p className='input-error'>{errors.about}</p>}
            </div>

            <div className="my-account-form__button">
                <button type="submit" className="row-input-button">{dictionary.resources.btnSave}</button>
            </div>
        </Form>
    )
};
const FormikApp = withFormik({

    handleSubmit(values, { props }) {
        props.saveTeacherData(
            props.teacherData.id,
            values.login || props.teacherData.login,
            values.email || props.teacherData.email,
            values.phone || props.teacherData.phone,
            values.about || props.teacherData.about,
        );
        props.changeButtonAccount()
    },
    validationSchema : props => Yup.object().shape({
        login: Yup.string()
            .matches(/^(?![0-9])/, props.dictionary.validation.cannotStartNumber)
            .matches(/^[a-zA-Z0-9]+$/, props.dictionary.validation.onlyLettersAndNumbers)
            .min((5), props.dictionary.validation.minLengthError)
            .max((30), props.dictionary.validation.maxLengthError30),
        email: Yup.string()
            .email(props.dictionary.validation.emailError),
        phone: Yup.string()
            .matches(/^[\+][38][0-9]+$/, props.dictionary.validation.phoneError)
            .min((13), props.dictionary.validation.maxLengthError13),
    }),
})(AccountForm);

export default React.memo(FormikApp)
