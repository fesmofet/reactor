const express = require('express')
const mongoose =require('mongoose')
const User = require('./models/user')
const bodyParser = require('body-parser');
const helper = require('./helpers/mongoHelpers');
const constants = require('./static/constants');

const app = express()

app.use((req, res, next) => helper.cors(req, res, next));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));
mongoose.set('useFindAndModify', false);

async function start() {
    try {
      await mongoose.connect(constants.pathMongo.url, {useUnifiedTopology: true, useNewUrlParser: true})
      app.listen(constants.pathMongo.PORT, () => {
        console.log(`Server is running on port ${constants.pathMongo.PORT}`)
    })
    } catch(e) {
      console.log(e);
    }
    
}
start();

app.post(constants.pathMongo.regData, (req, res) => helper.regData(req, res, User))

app.post(constants.pathMongo.sessionData, (req, res) => helper.sessionData(req, res, User));
app.post(constants.pathMongo.addGeo, (req, res) => helper.addGeo(req, res, User));
//Timers
app.post(constants.pathMongo.studentsTimer, (req, res) => helper.studentsTimer(req, res, User));
app.post(constants.pathMongo.paintTimer, (req, res) => helper.paintTimer(req, res, User));
app.post(constants.pathMongo.calcTimer, (req, res) => helper.calcTimer(req, res, User));
app.post(constants.pathMongo.accountTimer, (req, res) => helper.accountTimer(req, res, User));
app.post(constants.pathMongo.currencyTimer, (req, res) => helper.currencyTimer(req, res, User));
app.post(constants.pathMongo.converterTimer, (req, res) => helper.converterTimer(req, res, User));
// Click buttons
app.post(constants.pathMongo.createInc, (req, res) => helper.createInc(req, res, User));
app.post(constants.pathMongo.updateInc, (req, res) => helper.updateInc(req, res, User));
app.post(constants.pathMongo.deleteInc, (req, res) => helper.deleteInc(req, res, User));
app.post(constants.pathMongo.clearInc, (req, res) => helper.clearInc(req, res, User));
app.post(constants.pathMongo.chatInc, (req, res) => helper.chatInc(req, res, User));
app.post(constants.pathMongo.updateLogin, (req, res) => helper.updateLogin(req, res, User));
//GET all info
app.get(constants.pathMongo.getData, (req, res) => helper.getData(req, res, User))

