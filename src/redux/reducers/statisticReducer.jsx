import actionTypes from '@/constants/actionTypes';


const initialState = {
  users: [],

};

export const statisticReducer = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.SAVE_STATISTIC: return {
      ...state,
      users: action.payload,
    };

    default:
      return state;
  }
};
