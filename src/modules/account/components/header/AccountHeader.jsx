import React from 'react';
import Logo from './components/Logo';
import ControlButtons from './components/ControlButtons';
import { fullScreen } from "@utils/utils"

import {SocketConnection} from "../../../main-app/socketConnection"

export default class Header extends React.Component {
  constructor (props) {
    super(props)
    this.myHeader = null;
  }
  toggleSettingsModalWindow = () => {
    const { toggleModalWindow } = this.props;
    toggleModalWindow({ type: 'settings', data: null });
  };
  logoutHandler =() => {
    SocketConnection.getSocket().send(JSON.stringify({
      login: localStorage.getItem("UserLogin"),
      command: "USER_LOGOUT",
    }));
    
    this.props.logout()
  }

  render() {
    const { history} = this.props;
   
    return (
      <div className="account-header">
        <nav className="account-navigation">
          <Logo className="header__logo" alt="" src="./assets/logo.png"/>
          <div className="header__control-wrapper">
            <ControlButtons
              className="header__control-buttons control-buttons"
              buttons={[
                {
                  id: 1,
                  className: 'control-buttons__settings',
                  callback: this.toggleSettingsModalWindow,
                },
              ]}
            />
            <button onClick={() => history.push('/main/students')} className="back__button"/>
            <button onClick={fullScreen} className="fullscreen__button"/>
            <button onClick={this.logoutHandler} className="logout__button"/>
          </div>
        </nav>
      </div>
    );
  }
}

