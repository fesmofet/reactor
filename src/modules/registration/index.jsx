import { connect } from 'react-redux';
import Component from './Registration';

const mapStateToProps = state => ({
  inputs: state.config.inputs,
  buttons: state.config.buttonRegistration,
  dictionary: state.translates.dictionary,
});

const mapDispatchToProps = dispatch => ({});

export default connect(mapStateToProps, mapDispatchToProps)(Component);
