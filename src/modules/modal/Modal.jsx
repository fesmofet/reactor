import React from 'react';
import SettingsModal from './components/settingsModal/index.jsx';
import ModalStudents from './components/students/index';
import ForgotPassword from './components/forgotPassword/index.jsx';

import './Modal.less';

export default class Modal extends React.Component {
  render() {
    const { modals } = this.props;
    return (
      <>
        <SettingsModal isOpen={modals.settings.isOpen} />
        <ForgotPassword isOpen={modals.forgotPassword.isOpen} />
        <ModalStudents isOpen={modals.students.isOpen} />
      </>
    );
  }
}
