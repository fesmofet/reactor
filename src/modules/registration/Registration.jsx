import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import RegistrationForm from "./components/registrationForm/RegistrationForm"
import Header from '../header/index';
import './registration.less';

export default class Registration extends Component {
  
  render() {
    const { inputs, buttons, dictionary, history } = this.props;
    return (
      <div className="wrapper-registration">
        <Header />

        <div className="container-registration">
          <div>
            <span>{dictionary.resources.registration}</span>
          </div>
            <RegistrationForm dictionary={dictionary} history={history} {...this.props}/>
          <div>
            <Link to="/">{dictionary.dialogs.backToAuth}</Link>
          </div>
        </div>

      </div>
    );
  }
}
