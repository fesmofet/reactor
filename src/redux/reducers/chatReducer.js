import actionTypes from '@/constants/actionTypes';


const initialState = {
  isPrivateMode: false,
  users: [],
  messages: [],
  currentLogin: null,
};

export const chatReducer = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.SET_CURRENT_LOGIN: return {
        ...state,
        currentLogin: action.payload,
      };
    case actionTypes.TOGGLE_IS_PRIVATE_MODE: return {
      ...state,
      isPrivateMode: !state.isPrivateMode,
    };
    case actionTypes.ADD_USER: return {
        ...state,
        users: action.payload,
      };
      case actionTypes.ADD_MESSAGE: return {
        ...state,
        messages: [
            ...state.messages,
            action.payload,
        ],
      };
    default:
      return state;
  }
};
