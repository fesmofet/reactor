import {connect} from "react-redux";
import Content from './Students';

const mapStateToProps = state => ({
    dictionary: state.translates.dictionary,
    modals: state.modals,
});
const mapDispatchToProps = dispatch => ({

});

export default connect(mapStateToProps, mapDispatchToProps)(Content);
