import React from 'react';
import PropTypes from 'prop-types';

const Logo = (props) => {
  const { className, src, alt } = props;
  return (
    <div className={className}>
      <img src="../../../../../../assets/logo.png" alt={alt} />
    </div>
  );
};

Logo.propTypes = {
  alt: PropTypes.string.isRequired,
  src: PropTypes.string.isRequired,
  className: PropTypes.string.isRequired,
};

export default React.memo(Logo);
