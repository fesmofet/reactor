import React from 'react';
import Tab from "./components/tab/Tab.jsx";
import TableRow from "./components/tableRow/TableRow.jsx";
import EditableRow from "./components/editableRow/EditableRow.jsx";
import TableCaption from "./components/tableCaption/TableCaption";
import * as requests from '../../../../../../utils/requests'
import path from "@constants/path"
import "./studentsTable.less"

export default class StudentsTable extends React.Component {

    componentDidMount() {
       
        requests.getRenderData(this.props.teacherData.id, this.saveData, 0);
    }

    changeActiveTab = event => {
        const newActiveTab = event.currentTarget.accessKey;
        this.props.changeActiveTab(newActiveTab)
        requests.getRenderData(this.props.teacherData.id, this.saveData, newActiveTab);
    };

    saveData = (tabs, students) => {
        this.props.addTabs(tabs)
        this.props.addStudents(students)
    };

    handleDeleteTab = (event, groupId) => {
        event.stopPropagation();
        let deleteTabConfirmationText = this.props.dictionary.dialogs.deleteTab;
        if (window.confirm(deleteTabConfirmationText)) {
            requests.deleteGroupById(groupId)
                .then(() => {
                    requests.getRenderData(this.props.teacherData.id, this.saveData, 0)
                })
                .then(() => {
                    this.props.changeActiveTab(0)
                });
        }
    };

    handleDelete = (itemId, name, surname, age, city) => {
        requests.deleteStudentById(itemId)
            .then(() => {
                requests.getRenderData(this.props.teacherData.id, this.saveData, this.props.activeTab)
            })
        requests.clickCounter(path.deleteCl)
        this.props.deleteNotification(event, name, surname, age, city)
    };

    handleSave = (id, fn, ln, age, ht) => {
        requests.updateStudentInfo(id, fn, ln, age, ht)
            .then(() => {
                requests.getRenderData(this.props.teacherData.id, this.saveData, this.props.activeTab)
            })
            .then(this.props.cancelUpdate(id))
        requests.clickCounter(path.updateCl)
    };

    handleCancel = itemId => {
        this.props.cancelUpdate(itemId)
    };

    handleUpdate = itemId => {
        this.props.updateTableRow(itemId)
    };

    handleAddNewTab = () => {
        requests.addNewGroup(this.props.teacherData.id)
            .then(() => {
                requests.getRenderData(this.props.teacherData.id, this.saveData, 0)
            })
    };

    handleRenameTab = (event) => {
        const newActiveTab = event.currentTarget.accessKey;
        this.props.setTabAsRenamed(Number(newActiveTab));
    };

    handleSaveTabName = (groupId, event) => {
        if(!event.currentTarget.value) {
            return alert(this.props.dictionary.dialogs.emptyString)
        }
        requests.renameGroup(groupId, event.currentTarget.value)
            .then(() => this.props.completeTabRenamed(null))
            .then(() => {
                requests.getRenderData(this.props.teacherData.id, this.saveData, this.props.activeTab)
            })
    };
    

    render() {

        const {tabs, items, activeTab, renamedTabNumber, dictionary} = this.props;
        const {changeActiveTab} = this;
        return (
            <div className="table">
                <div className="table-caption">
                    <span>{dictionary.resources.students}</span>
                </div>
                <div className="tabs">
                    
                    {tabs.length && tabs.map(element => {
                        return <Tab
                            groupId={element.groupId}
                            key={element.groupId}
                            name={element.name}
                            isActive={activeTab === element.sequenceNumber}
                            sequenceNumber={element.sequenceNumber}
                            cantBeDeleted={tabs.length !== 1}
                            tabType={tabs.type}
                            renameTab={this.handleRenameTab}
                            isTabRenamed={renamedTabNumber === element.sequenceNumber}
                            changeActiveTab={changeActiveTab}
                            onDeleteTab={this.handleDeleteTab}
                            saveTabName={this.handleSaveTabName}/>
                    })}
                    {tabs.length < 3 &&
                    <button className="add-new-tab" onClick={this.handleAddNewTab} >+</button> }
                </div>
                <TableCaption dictionary={dictionary}/>
                <div className="rows">
                    {items.length && items.map((item, index) => {
                        if (!this.props.currentEditItems.includes(item.id)) {
                            return <TableRow
                                tableIndex={index + 1}
                                id={item.id}
                                key={item.id}
                                item={item}
                                updateText={dictionary.dialogs.update}
                                deleteText={dictionary.dialogs.delete}
                                onUpdate={this.handleUpdate}
                                onDelete={this.handleDelete}/>
                        } else {
                            return <EditableRow
                                {...this.props} 
                                tableIndex={index + 1}
                                id={item.id}
                                key={item.id}
                                item={item}
                                onSave={this.handleSave}
                                onCancel={this.handleCancel}
                                saveText={dictionary.dialogs.save}
                                cancelText={dictionary.dialogs.cancel}/>
                        }
                    })
                    }
                </div>
            </div>
        )
    }
}

