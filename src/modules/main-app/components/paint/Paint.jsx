import React from "react";
import("./paint.less");
import ControlPanelPaint from "./components/ControlPanelPaint";
import { DrawController} from "./helpers/helperPaint";
import Header from '../header/index';


export default class Paint extends React.Component {

  componentDidMount = () => {
    this.paint = new DrawController();
    this.paint.initialization();
  };

  render() {
    const {dictionary, history} = this.props;
    return (
        <>
          <main className="main">
            <Header
                history={history}/>
            <div className="wrapper-paint" id="wrapper1">
              <ControlPanelPaint dictionary={dictionary}/>
              <div className="canvas-wrapper">
                <div className="canvas1">
                  <h2>{dictionary.resources.paint}</h2>
                  <canvas className="canvas-paint" id="canvas1" width='600' height='400'/>
                </div>
              </div>
            </div>
          </main>
        </>
    );
  }
};
