import React from 'react';
import PropTypes from 'prop-types';
import Modal from 'react-modal';
import './ForgotPassword.less';
import CustomInput from '@libs/CustomInput/CustomInput';
import * as forgottenRequest from '@utils/requests';

export default class ForgotPassword extends React.Component {
  static propTypes = {
    dictionary: PropTypes.object.isRequired,
    toggleModalWindow: PropTypes.func.isRequired,
  };

  forgottenRef = {
    loginForgotten: React.createRef(),
    keywordForgotten: React.createRef(),
    passwordRecover: React.createRef(),
    getPassword: React.createRef(),
    message: React.createRef(),
  };

  toggleSettingsWindow = () => {
    const { toggleModalWindow } = this.props;
    toggleModalWindow({ type: 'forgotPassword', data: null });
  };

  getCurrentValue = () => ({
    loginForgotten: this.forgottenRef.loginForgotten.current.value,
    keywordForgotten: this.forgottenRef.keywordForgotten.current.value,
  });

  getPassword = () => {
    const { dictionary } = this.props;
    const data = this.getCurrentValue();
    forgottenRequest.getPasswordValue(data, this.forgottenRef, dictionary);
  };

  render() {
    const { dictionary, inputsForgottenPassword } = this.props;
    return (
      <Modal
        isOpen={this.props.isOpen}
        style={this.props.customStyles}
        onRequestClose={this.toggleSettingsWindow}
        ariaHideApp={false}
      >
        <div className="modal">
          <header className="modal__header-modal">
            <h3>{dictionary.resources.forgottenPassword}</h3>
          </header>
          <div className="container-forgotten">
            <div className="container-forgotten__button__close">
              <button onClick={this.toggleSettingsWindow}>X</button>
            </div>
            {inputsForgottenPassword.map((input) => (
              <CustomInput
                id={input.id}
                key={input.id}
                inputRef={this.forgottenRef[input.id]}
                type={input.type}
                dictionary={dictionary}
                labelText={input.labelText}
                className={input.className}
                placeholderKey={input.placeholderKey}
                disabled={input.disabled ? input.disabled : null}
              />
            ))}
            <div className="container-forgotten__button">
              <button onClick={this.getPassword}>
                {dictionary.resources.getPassword}
              </button>
            </div>
          </div>
          <div className="" ref={this.forgottenRef.message} />
        </div>
      </Modal>
    );
  }
}
