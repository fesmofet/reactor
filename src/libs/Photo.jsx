import React from 'react';
import PropTypes from 'prop-types';

const Photo = (props) => {
    const { className, src, alt } = props;
    return (
        <div className={className}>
            <img src={src} alt={alt} />
        </div>
    );
};

Photo.propTypes = {
    alt: PropTypes.string.isRequired,
    src: PropTypes.string.isRequired,
    className: PropTypes.string.isRequired,
};

export default React.memo(Photo);