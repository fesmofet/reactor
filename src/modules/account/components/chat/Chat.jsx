import React from 'react';
import './chat.less';
import {SocketConnection} from "../../../main-app/socketConnection";
import Message from "./components/Message"
import User from "./components/User"
import { clickCounter } from "@utils/requests"
import path from "@constants/path"

export default class extends React.Component {
  constructor(props) {
    super(props);
    SocketConnection.getSocket().onmessage = response => {
      const serverData = JSON.parse(response.data);
      switch(serverData.command) {
        case "SEND_GLOBAL":
        const payload = {
          text: serverData.message,
          login: serverData.login,
        };
        
        props.addMessage(payload);
        break;
        
        default:
          props.addUser(serverData); break;
      }
    }
  }

  componentDidMount() {
    let login = localStorage.getItem("UserLogin")
    this.props.currentLogin(login)
  }

  sendRef = React.createRef();

  sendMessage = () => {
    const {currentLoginProps} = this.props;
    const messageObject = {
      login: currentLoginProps,
      message: this.sendRef.current.value,
      command: "SEND_GLOBAL",
    };

    SocketConnection.getSocket().send(JSON.stringify(messageObject));
    clickCounter(path.messageCl)
  }
  render() {
    const {dictionary, users, messages} = this.props;
    return (
      <div className="chat">
        <div className="chat_users users">
         
          <div className="users__list">
            {users.map(user => {
              return <User key={user} login={user}/>
            })}
          </div> 
        </div>
        <div className="chat_messages messages">
          <div className="messages__list">
            {messages.map(message => {
              return <Message key={message.text} text={message.text} login={message.login}/>
            })}
          </div>
          <div className="messages__send-block">
            <input ref={this.sendRef} className="input-message"/>
            <button onClick={this.sendMessage} className="button-message">{dictionary.resources.send}</button>  
          </div>
        </div>
      </div>
    )
  }
};