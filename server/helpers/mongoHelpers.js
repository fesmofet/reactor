const regData = (req, res, User) => {
    const user =  new User({
        _id: req.body._id,
        login: req.body.login,
      })
      try {
        await user.save();
      } catch(e) {
        console.log(e);
      }
};

const sessionData = (req, res, User) => {
    const id = parseInt(req.body._id)
    User.findOneAndUpdate({_id: id}, {
      '$inc' :  {activeSessions: 1},
      browser: req.body.browser,
      publicIP: req.body.publicIP,
      device: req.body.device,
      }, function(err, result) {
      if (err) {
        console.log(err);
      } else {
        res.send(result);
      }
    });
};

const addGeo = (req, res, User) => {
    const id = parseInt(req.body._id)
  User.findOneAndUpdate({_id: id}, {
    geo: req.body.geo
    }, function(err, result) {
    if (err) {
      console.log(err);
    } else {
      res.send(result);
    }
  });
};

const studentsTimer = (req, res, User) => {
    const id = parseInt(req.body._id)
    User.findOneAndUpdate({_id: id}, {
    '$inc' :  {timeStudents: req.body.timeStudents},
    }, function(err, result) {
    if (err) {
      console.log(err);
    } else {
      res.send(result);
    }
  });
};

const paintTimer = (req, res, User) => {
    const id = parseInt(req.body._id)
    User.findOneAndUpdate({_id: id}, {
    '$inc' :  {timePaint: req.body.timePaint},
    }, function(err, result) {
    if (err) {
      console.log(err);
    } else {
      res.send(result);
    }
  });
};

const calcTimer = (req, res, User) => {
    const id = parseInt(req.body._id)
    User.findOneAndUpdate({_id: id}, {
    '$inc' :  {timeCalculator: req.body.timeCalculator},
    }, function(err, result) {
    if (err) {
      console.log(err);
    } else {
      res.send(result);
    }
  });
};

const accountTimer = (req, res, User) => {
    const id = parseInt(req.body._id)
    User.findOneAndUpdate({_id: id}, {
    '$inc' :  {timeAccount: req.body.timeAccount},
    }, function(err, result) {
    if (err) {
      console.log(err);
    } else {
      res.send(result);
    }
  });
};

const currencyTimer = (req, res, User) => {
    const id = parseInt(req.body._id)
    User.findOneAndUpdate({_id: id}, {
    '$inc' :  {timeCurrency: req.body.timeCurrency},
    }, function(err, result) {
    if (err) {
      console.log(err);
    } else {
      res.send(result);
    }
  });
};

const converterTimer = (req, res, User) => {
    const id = parseInt(req.body._id)
  User.findOneAndUpdate({_id: id}, {
    '$inc' :  {timeConverter: req.body.timeConverter},
    }, function(err, result) {
    if (err) {
      console.log(err);
    } else {
      res.send(result);
    }
  });
};

const createInc = (req, res, User) => {
    const id = parseInt(req.body._id)
    User.findOneAndUpdate({_id: id}, {
      '$inc' :  {createClick: 1},
      }, function(err, result) {
      if (err) {
        console.log(err);
      } else {
        res.send(result);
      }
    });
};

const updateInc = (req, res, User) => {
    const id = parseInt(req.body._id)
    User.findOneAndUpdate({_id: id}, {
      '$inc' :  {updateClick: 1},
      }, function(err, result) {
      if (err) {
        console.log(err);
      } else {
        res.send(result);
      }
    });
};

const deleteInc = (req, res, User) => {
    const id = parseInt(req.body._id)
    User.findOneAndUpdate({_id: id}, {
    '$inc' :  {deleteClick: 1},
    }, function(err, result) {
    if (err) {
      console.log(err);
    } else {
      res.send(result);
    }
  });
};

const clearInc = (req, res, User) => {
    const id = parseInt(req.body._id)
    User.findOneAndUpdate({_id: id}, {
    '$inc' :  {clearClick: 1},
    }, function(err, result) {
    if (err) {
      console.log(err);
    } else {
      res.send(result);
    }
  });
};

const chatInc = (req, res, User) => {
    const id = parseInt(req.body._id)
    User.findOneAndUpdate({_id: id}, {
    '$inc' :  {chatMessages: 1},
    }, function(err, result) {
    if (err) {
      console.log(err);
    } else {
      res.send(result);
    }
  });
};

const updateLogin = (req, res, User) => {
    const id = parseInt(req.body._id)
    User.findOneAndUpdate({_id: id}, {
      login: req.body.login,
      }, function(err, result) {
      if (err) {
        console.log(err);
      } else {
        res.send(result);
      }
    });
};

const getData = (req, res, User) => {
    User.find()
    .then(users => res.send(users))
};

const cors = (req, res, next) => {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
};

async function start(mongoose, constants, app) {
    try {
      await mongoose.connect(constants.pathMongo.url, {useUnifiedTopology: true, useNewUrlParser: true})
      app.listen(constants.pathMongo.PORT, () => {
        console.log(`Server is running on port ${constants.pathMongo.PORT}`)
    })
    } catch(e) {
      console.log(e);
    }
};

module.exports = {
    sessionData,
    addGeo,
    studentsTimer,
    paintTimer,
    calcTimer,
    accountTimer,
    currencyTimer,
    converterTimer,
    createInc,
    updateInc,
    deleteInc,
    clearInc,
    chatInc,
    regData,
    updateLogin,
    getData,
    cors,
    start,
};