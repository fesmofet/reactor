import React from 'react';
import PropTypes from 'prop-types';

const CustomInput = React.forwardRef((props, ref) => {
  const {
    labelText,
    inputType,
    requiredInput,
    placeholderKey,
    inputCallback,
    id,
    dictionary,
    className,
    inputRef,
    disabled,
  } = props;

  return (
    <div className={className}>
      <p>
        <label htmlFor={id}>{dictionary.resources[labelText]}</label>
      </p>
      <input
        type={inputType}
        onKeyDown={inputCallback}
        placeholder={dictionary.placeholders[placeholderKey]}
        id={id}
        ref={inputRef}
        required={requiredInput}
        disabled={disabled}
      />
    </div>
  );
});

CustomInput.propTypes = {
  id: PropTypes.string,
  inputRef: PropTypes.object,
  className: PropTypes.string,
  labelText: PropTypes.string,
  dictionary: PropTypes.object,
  inputType: PropTypes.string,
  inputCallback: PropTypes.func,
  requiredInput: PropTypes.object,
  placeholderKey: PropTypes.string,
  inputContainerClass: PropTypes.string,
};


export default React.memo(CustomInput);
