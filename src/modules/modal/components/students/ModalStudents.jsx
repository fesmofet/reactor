import React from 'react';
import PropTypes from 'prop-types';
import Modal from 'react-modal';
import Select from '@libs/select/Select';
import * as utils from '@utils/utils';
import './ModalSettings.less';
import {deleteGroupById, addNewGroup, getRenderData, getGroupsFromServer} from '@utils/requests'
import { Promise } from 'mongoose';

export default class ModalStudents extends React.Component {
  static propTypes = {
    options: PropTypes.array.isRequired,
    dictionary: PropTypes.object.isRequired,
    changeLocale: PropTypes.func.isRequired,
    toggleModalWindow: PropTypes.func.isRequired,
  };
  checkboxRef = React.createRef();

  toggleCheckbox = () => {
    this.props.toggleNotification(this.checkboxRef.current.checked)
  }
  
  onChange = (event) => {
    const { changeLocale } = this.props;
    const selectedLocale = event.currentTarget.value;
    utils.setBodyDirection(selectedLocale === 'ar' ? 'rtl' : 'ltr');
    utils.setDataLS('locale', selectedLocale);
    changeLocale(selectedLocale);
  };

  toggleSettingsWindow = () => {
    const { toggleModalWindow } = this.props;
    toggleModalWindow({ type: 'students', data: null });
  };

  handleResetAll =() => {
    if(window.confirm(`${this.props.dictionary.dialogs.reset}`)){
      this.props.tabs.forEach(item => deleteGroupById(item.groupId))
      addNewGroup(this.props.id)
      localStorage.setItem('locale', 'en');
      location.reload()
    }
    
  }

  render() {
    return (
      <Modal
        isOpen={this.props.isOpen}
        style={this.props.customStyles}
        onRequestClose={this.toggleSettingsWindow}
        ariaHideApp={false}
      >
        <div className="settings-modal-wrapper">
          <header className="settings-modal-wrapper__header-modal">
            <span>{this.props.dictionary.resources.settings}</span>
            <button className="custom-button" onClick={this.toggleSettingsWindow}>x</button>
          </header>
          <div className="settings-modal-wrapper__content-modal">
            <Select className="" dictionary={this.props.dictionary} options={this.props.options} callback={this.onChange} />
          </div>
          <p>{this.props.dictionary.resources.offNotification}</p>
          <label className="switch">
            <input type="checkbox" ref={this.checkboxRef} onChange={this.toggleCheckbox} checked={this.props.modals.notificationOff}/>
            <span className="slider round"></span>
          </label>
          <button onClick={this.handleResetAll} className="reset-button">{this.props.dictionary.resources.reset}</button>
        </div>
      </Modal>
    );
  }
}
