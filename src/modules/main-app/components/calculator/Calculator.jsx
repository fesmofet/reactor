import React, { Component } from 'react';
import App from './containers/App'
import registerServiceWorker from './utils/registerServiceWorker'
import { pageTimer } from '@/utils/requests';
import path from "@constants/path"
import Header from '../header/index'

registerServiceWorker()

export default class ScientificCalculator extends Component {
  calcCount;
  count = 0;
  componentDidMount() {
      this.calcCount = setInterval(() => {
          this.count++
      }, 10)
  }
  componentWillUnmount() {
    clearInterval(this.calcCount)
    pageTimer(this.count, path.calcTimer, path.timeCalculator)
  }
  render() {
    const { history } = this.props
    return (
      <>
        <Header history={history}/>
        <App />
      </>
    );
  }
}