import React, { Component } from 'react';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Redirect,
} from 'react-router-dom';
import Authorization from '../authorization';
import MainApp from '../main-app/index';
import Registration from '../registration/index';
import Admin from '../admin/index';
import Modal from '../modal/Modal';


export default class Layout extends Component {

  render() {
    const checkLS = localStorage.getItem('isLoggedIn') === 'true';
    return (
      <Router>
        <div className="wrapper">
          <React.Fragment>
            <Switch>
              <Route exact path="/" component={Authorization} />
              <Route path="/registration" component={Registration} />
              {checkLS ? <Route path="/main" component={MainApp} /> : null}
              {checkLS ? <Route path="/admin" component={Admin} /> : null}
              <Redirect to="/" />
            </Switch>
            <Modal modals={this.props.modals} />
          </React.Fragment>
        </div>

      </Router>
    );
  }
}
