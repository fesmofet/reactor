import ControlPanel from "./ControlPanel.jsx";
import {connect} from "react-redux";
import * as actions from "../../../../../../constants/actionCreators";


const mapStateToProps = state => ({
    teacherData: state.students.teacherData,
    teacherGroups: state.students.teacherGroups,
    dictionary: state.translates.dictionary,
    tabs: state.students.tabs,
    items: state.students.items,
    currentEditItems: state.students.currentEditItems,
    activeTab: state.students.activeTab,
});

const mapDispatchToProps = dispatch => ({
    addTabs: payload => dispatch(actions.addTabs(payload)),
    addStudents: payload => dispatch(actions.addStudents(payload)),
});

export default connect(mapStateToProps, mapDispatchToProps)(ControlPanel);