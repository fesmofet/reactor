import { connect } from 'react-redux';
import Component from './Chat.jsx';
import * as actions from '@constants/actionCreators'


const mapStateToProps = state => ({
  users: state.chatData.users,
  messages: state.chatData.messages,
  dictionary: state.translates.dictionary,
  currentLoginProps: state.chatData.currentLogin,
});

const mapDispatchToProps = dispatch => ({
  addUser: payload => dispatch(actions.addUser(payload)),
  addMessage: payload => dispatch(actions.addMessage(payload)),
  toggleIsPrivateMode: () => dispatch(actions.toggleIsPrivateMode()),
  currentLogin: payload => dispatch(actions.currentLogin(payload)),
});

export default connect(mapStateToProps, mapDispatchToProps)(Component);
