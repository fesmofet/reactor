const local = localStorage.getItem('isLoggedIn') ? local : localStorage.setItem('isLoggedIn', false);

const getAuthorize = () => {
  const setTrue = localStorage.setItem('isLoggedIn', true);
  local || setTrue;
  return true;
};

const getLogout = () => {
  
  localStorage.setItem('isLoggedIn', false);
  // localStorage.clear();
  return false;
};
const initialState = {
  isLoggedIn: local,
};

export const authorizeReducer = (state = initialState, action) => {
  switch (action.type) {
    case 'LOG_IN':
      return {
        ...state,
        isLoggedIn: getAuthorize(),
      };
    case 'LOG_OUT':
      return {
        ...state,
        isLoggedIn: getLogout(),
      };
    default:
      return state;
  }
};
